<br />
<div align="left">
  <h2 align="left">libgc</h2>
  <p align="left">
    Linear solver based either on [sparse](https://www.netlib.org/sparse/readme) (NETLIB - User guide available at https://www.netlib.org/sparse/spdoc), or 
    [modulef](https://www.rocq.inria.fr/modulef/english.html) (&copy; 1999 INRIA), both EPL v2.0 friendly. 
    <br />
    <br />
    Library developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
    <br />
    Nicolas GALLOIS <a href="mailto:nicolas.gallois@minesparis.psl.eu">nicolas.gallois@minesparis.psl.eu</a>
  </p>
</div>

## Copyright

[![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

&copy; 2022 Contributors to the libgc library.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 
which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.
