      subroutine maxtab (tab,n,rm)
        include 'dprec.h'

        include 'entsor.h'

c        data ipass/1/
c        save ipass
c ----------------------------------------------------------------------
c maximum en valeur absolue d'un tableau
c
      dimension tab(*)
      common /contro/ impres
      if (impres.ge.1) write (imp,*) ' >> maxtab'
      rm=0.
      do 100 i=1,n
         rr=tab(i)*tab(i)
         if (rr.gt.rm) rm=rr
100   continue
      rm = sqrt (rm)
      end
