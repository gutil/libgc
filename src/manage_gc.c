/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: manage_gc.c
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "spmatrix.h"
#include <omp.h>
#include "libprint.h"
#include "GC.h"

void GC_init_gc(s_gc *pgc,int type_appl)
{
  pgc->sw_int[NS_GC]= NSYS_GC;
  pgc->sw_int[B0_GC]=INITB0_GC; 
  pgc->sw_int[NIVPRECOND_GC]=NIVPROP_GC;
  pgc->sw_int[LPRECOND_GC]=LMAC5PROP_GC;
  pgc->sw_int[NITMAX_GC]=NITER_GC;
  pgc->sw_int[TRAIT_GC]=ITRAIT_GC;
  pgc->sw_int[STGRC_GC]=ISTGRC_GC;
  pgc->sw_int[CALCPC_GC]=ICALCPC_GC;
  pgc->sw_int[ECO_GC]=IECO_GC;
  pgc->sw_int[PRECONDMAX_GC]=DEGMAX_GC;
  pgc->sw_int[APPL_NB]=type_appl;
  pgc->epsgc=EPS_GC;
  

}

s_gc *GC_create_gc(int type_appl)
{

  s_gc *pgc;

  pgc = new_gc();
  bzero((char *)pgc,sizeof(s_gc));
  GC_init_gc(pgc,type_appl);

  return pgc;

}

int *GC_create_mat_int(int isize)
{
  int *mat;

  mat=((int*) malloc(isize*sizeof(int)));
  bzero((char *)mat,isize*sizeof(int));

  return mat;
}

double *GC_create_mat_double(int isize)
{
  double *mat;

  mat=((double*) malloc(isize*sizeof(double)));
  bzero((char *)mat,isize*sizeof(double));

  return mat;

}

void GC_check_consistency(s_gc *pgc, FILE *fp)
{
  int *mat4,*mat5,*ndl,*lmat5;
  double *mat6;
  int i,col_diag;
  
  mat4=pgc->mat4;
  mat5=pgc->mat5;
  ndl=&pgc->ndl;
  lmat5=&pgc->lmat5;
  mat6=pgc->mat6;

  for (i=1;i<=*ndl;i++){
    col_diag=mat4[i];
    if (mat5[col_diag-1]!=i)
      LP_error(fp,"libgc%4.2f %s in %s l%d : The LHS is not defined properly. The diagonal number is not the last one of mat5.\nIn the LHS line %d, the last defined coefficient is the %d one, which is not the diagonal one %d\n The matricial system is not built conformly to the libgc requirement, which imposes the last term of a raw being the diagonal one. Check the way mat5 and mat6 are filled and read libgc user guide\n",VERSION_GC,__func__,__FILE__,__LINE__,i,mat5[col_diag],i);
  }
  
}

// NG : 09/06/2021
/**\fn void GC_configure_gc(s_gc*, int*, FILE*)
 *\brief Function used either to set or update a specific configuration 
 of the GC solver. Only depends either on a global time-step
 counter (if in transient mode), or an iteration counter (in case of
 steady state calculations).
 Note : Iteration counter (single integer) is used via its memory adress.
 *\return void
 */
void GC_configure_gc(s_gc *pgc, int* nit, FILE* fpout)
{
    if (*nit > 1) 
    {
        pgc->sw_int[B0_GC] = 1;
    }

   // GC_print_configuration_gc(pgc,nit,fpout); // NG check
}


/**\fn void GC_print_configuration_gc(s_gc*, int*, FILE*)
 *\brief Quick debug/print function to check GC solver config.
 *\return void
 */
void GC_print_configuration_gc(s_gc *pgc, int* nit, FILE* fpout)
{
    int i;
    LP_printf(fpout,"\n GC solver configuration at function call %d :\n",*nit);
    for (i = 0; i < MSG_GC; i++)
        LP_printf(fpout,"%s = %d\n",GC_name_config_param(i,fpout),pgc->sw_int[i]);
    LP_printf(fpout,"\n");
}


/* NG : 09/06/2021 : Modification in F90 gc_solve_() function, the 
preconditionning level calculated by the solver at each call is set 
as the minimum default preconditionning level for the next iteration. 
Done in order to save some calculation time. As of now, it cannot regress 
to a lower level from one function call to the next one.
*/ 
void GC_solve_gc(s_gc *pgc, int iname, FILE* fpout)
{
  int *mat4,*mat5,*sw_int,*ndl,*lmat5,appl_nb;
  double *epsgc,*sol,*b,*mat6;
  char *name_iappl = NULL;
  mat4=pgc->mat4;
  mat5=pgc->mat5;
  ndl=&pgc->ndl;
  lmat5=&pgc->lmat5;
  sol=pgc->x;
  epsgc=&pgc->epsgc;
  mat6=pgc->mat6;
  b=pgc->b;
  appl_nb=pgc->appl_nb;
  sw_int=pgc->sw_int;
  name_iappl = GC_name_iappl(iname,fpout);

  LP_printf(fpout,"In libgc%4.2f : Solver application : %s. Preconditionning level set to : %d\n",VERSION_GC, name_iappl,sw_int[NIVPRECOND_GC]);
  gc_init_sys_(mat4,mat5,ndl);
  gc_solve_(mat6,lmat5,b,ndl,sol,sol,sw_int,epsgc);
  free(name_iappl);
}


void GC_clean_gc(void)
{
  purge_noms_fichiers_();
}

void GC_fill_sparse(s_gc *pgc,double *RHS_b,void *mat)
{
	
	int i, j;
	//int ind_mat6 = 0;

	omp_set_num_threads(10);
	#pragma omp parallel for shared(pgc,mat,RHS_b)     
	for(i = 1; i < pgc->ndl + 1; i++) // mat4 dimension
	{
        spREAL *pelement;
		int ncol;
		for(j = pgc->mat4[i-1]; j < pgc->mat4[i]; j++)
		{
			ncol = pgc->mat5[j] - 1;
			pelement = spGetElement(mat,i,ncol+1);
			*pelement = pgc->mat6[j];
			//ind_mat6++;
		}
		RHS_b[i] = pgc->b[i-1];		
	} 
}
