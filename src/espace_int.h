
	interface enregistre

	  module procedure enregistre_int
	  module procedure enregistre_flt
	  module procedure enregistre_log
	  module procedure enregistre_int_2D
	  module procedure enregistre_flt_2D

	end interface

	interface desenregistre

	  module procedure desenregistre_int
	  module procedure desenregistre_flt
	  module procedure desenregistre_log
	  module procedure desenregistre_int_2D
	  module procedure desenregistre_flt_2D

	end interface

	interface libere

	  module procedure libere_int
	  module procedure libere_flt
	  module procedure libere_log
	  module procedure libere_int_2D
	  module procedure libere_flt_2D

	end interface

	interface sauve_tab

	  module procedure sauve_tab_int
	  module procedure sauve_tab_flt
	  module procedure sauve_tab_log
	  module procedure sauve_tab_int_2D
	  module procedure sauve_tab_flt_2D

	end interface

	interface restore_tab

	  module procedure restore_tab_int
	  module procedure restore_tab_flt
	  module procedure restore_tab_log
 	  module procedure restore_tab_int_2D
 	  module procedure restore_tab_flt_2D

	end interface
