!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: alloue_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module alloue_mod

include 'gere_tab_int.h'

contains

! tableaux 1D

subroutine alloue_int (tab,long,message,val)

   use espace_mod

   implicit none
   integer(4) long,res
   integer(4),pointer :: tab(:)
   character(*) :: message
   integer(4),optional :: val
   include 'entsor.h'

   !include 'espace_int.h'

!
! tableau alloue s'il n'existe pas ou est trop petit
!
   if (associated (tab)) then
      if (size (tab) .lt. long) then
!        write (imp,*) ' desallocation (',message,')'
         deallocate (tab)
         nullify (tab)
      endif
   endif
   if (.not. associated (tab)) then
      allocate (tab(long),stat=res)
      call diag_alloc (res,message)
      if (present (val)) tab(1:long) = val
   endif
!
! enregistrement du tableau
!
   call enregistre (tab,trim(message),long)

end subroutine alloue_int

subroutine alloue_flt (tab,long,message,val,new)

   use espace_mod

   implicit none
   integer(4) long,res
   real(8),pointer :: tab(:)
   character(*) :: message
   real(8),optional :: val
   logical,optional :: new
   logical b_new
   include 'entsor.h'

   !include 'espace_int.h'

   b_new = .FALSE.
   if (associated (tab)) then
      if (size (tab) .lt. long) then
         ! deallocate (tab)
         ! nullify (tab)
         call desalloue_flt (tab)
      endif
   endif
   if (.not. associated (tab)) then
      allocate (tab(long),stat=res)
      call diag_alloc (res,message)
      if (present (val)) then
	 tab(1:long) = val
       endif
       b_new = .TRUE.
   endif
   if (present (new)) new = b_new
!
! enregistrement du tableau
!
   call enregistre (tab,trim(message),long)

end subroutine alloue_flt

subroutine desalloue_int (tab,message)

   use espace_mod

   implicit none
   integer(4) res
   integer(4),pointer :: tab(:)
   character(*),optional :: message
   character(100) :: mes
   character(100) :: mesdef = ' liberation de tableau '

   include 'entsor.h'

   !include 'espace_int.h'

   if (associated(tab)) then
      call desenregistre (tab)
      deallocate (tab,stat=res)
      mes = mesdef
      if (present (message)) mes = message
      call diag_desalloc (res,mes)
      nullify (tab)
   endif

end subroutine desalloue_int

subroutine desalloue_flt (tab,message)

   use espace_mod

   implicit none
   integer(4) res
   real(8),pointer :: tab(:)
   character(*),optional :: message
   character(100) :: mes
   character(100) :: mesdef = ' liberation de tableau '

   include 'entsor.h'

   !include 'espace_int.h'

   if (associated(tab)) then
      call desenregistre (tab)
      deallocate (tab,stat=res)
      mes = mesdef
      if (present (message)) mes = message
      call diag_desalloc (res,mes)
      nullify (tab)
   endif

end subroutine desalloue_flt

! tableaux 2D

subroutine alloue_int_2D (tab,dim1,dim2,message,val)

   use espace_mod

   implicit none
   integer(4) dim1,dim2,res
   integer(4),pointer :: tab(:,:)
   character(*) :: message
   integer(4),optional :: val
   include 'entsor.h'

   !include 'espace_int.h'

!
! tableau alloue s'il n'existe pas ou est trop petit
!
   if (associated (tab)) then
      if (size (tab) .lt. dim1*dim2) then
!        write (imp,*) ' desallocation (',message,')'
         deallocate (tab)
         nullify (tab)
      endif
   endif
   if (.not. associated (tab)) then
      allocate (tab(dim1,dim2),stat=res)
      call diag_alloc (res,message)
      if (present (val)) tab = val
   endif
!
! enregistrement du tableau
!
   call enregistre (tab,trim(message),dim1,dim2)

end subroutine alloue_int_2D

subroutine alloue_flt_2D (tab,dim1,dim2,message,val)

   use espace_mod

   implicit none
   integer(4) dim1,dim2,res
   real(8),pointer :: tab(:,:)
   character(*) :: message
   real(8),optional :: val
   include 'entsor.h'

   !include 'espace_int.h'

!
! tableau alloue s'il n'existe pas ou est trop petit
!
   if (associated (tab)) then
      if (size (tab) .lt. dim1*dim2) then
!        write (imp,*) ' desallocation (',message,')'
         deallocate (tab)
         nullify (tab)
      endif
   endif
   if (.not. associated (tab)) then
      allocate (tab(dim1,dim2),stat=res)
      call diag_alloc (res,message)
      if (present (val)) tab = val
   endif
!
! enregistrement du tableau
!
   call enregistre (tab,trim(message),dim1,dim2)

end subroutine alloue_flt_2D

subroutine desalloue_int_2D (tab,message)

   use espace_mod

   implicit none
   integer(4) res
   integer(4),pointer :: tab(:,:)
   character(*),optional :: message
   character(100) :: mes
   character(100) :: mesdef = ' liberation de tableau '


   !include 'espace_int.h'

   if (associated(tab)) then
      deallocate (tab,stat=res)
      mes = mesdef
      if (present (message)) mes = message
      call diag_desalloc (res,mes)
      nullify (tab)
      call desenregistre (tab)
   endif

end subroutine desalloue_int_2D

subroutine desalloue_flt_2D (tab,message)

   use espace_mod

   implicit none
   integer(4) res
   real(8),pointer :: tab(:,:)
   character(*),optional :: message
   character(100) :: mes
   character(100) :: mesdef = ' liberation de tableau '

   !include 'espace_int.h'

   if (associated(tab)) then
      deallocate (tab,stat=res)
      mes = mesdef
      if (present (message)) mes = message
      call diag_desalloc (res,mes)
      nullify (tab)
      call desenregistre (tab)
   endif

end subroutine desalloue_flt_2D

subroutine alloue_log (tab,long,message,val)

   use espace_mod

   implicit none
   integer(4) long,res
   logical,pointer :: tab(:)
   character(*) :: message
   logical,optional :: val
   include 'entsor.h'

   !include 'espace_int.h'

!
! tableau alloue s'il n'existe pas ou est trop petit
!
   if (associated (tab)) then
      if (size (tab) .lt. long) then
!        write (imp,*) ' desallocation (',message,')'
         deallocate (tab)
         nullify (tab)
      endif
   endif
   if (.not. associated (tab)) then
      allocate (tab(long),stat=res)
      call diag_alloc (res,message)
      if (present (val)) tab(1:long) = val
   endif
!
! enregistrement du tableau
!
   call enregistre (tab,trim(message),long)

end subroutine alloue_log

subroutine desalloue_log (tab,message)

   use espace_mod

   implicit none
   integer(4) res
   logical,pointer :: tab(:)
   character(*),optional :: message
   character(100) :: mes
   character(100) :: mesdef = ' liberation de tableau '

   !include 'espace_int.h'

   if (associated(tab)) then
      call desenregistre (tab)
      deallocate (tab,stat=res)
      mes = mesdef
      if (present (message)) mes = message
      call diag_desalloc (res,mes)
      nullify (tab)
   endif

end subroutine desalloue_log

end module alloue_mod
