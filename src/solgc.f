      subroutine solgc(
     #   rmat6,b,x,x0,initb0,niveau,long,
     3   ntdl,eps,itrait,istgrc,niter,i_cal_pc,ndsm,message,ieco,
     4   deg_max_gc,iappli)

c$ use OMP_LIB
        use GC_mod
        use fichier_mod
	use tempo_mod

        include 'dprec.h'
        include 'dump.h'

!include 'lecmac5_int.h'
	include 'connecte_int.h'

c-----------------------------------------------------------------------
c         
c                      GRADIENT CONJUGUE
c
c                   version double precision MODULEF 90
c
c-----------------------------------------------------------------------
c
c systeme d'equations:
c
c     long= nombre de termes non-nuls dans les matrices mac5 et rmac6
c     ntdl= nombre de noeuds
c     ndsm= nombre de seconds membres
c
c     x{.}= vecteur des inconnues
c     x0{.}= vecteur des inconnues initiales
c     b{.}= vecteur du second membre
c     rmat6= matrice compressee
c     mat5= pointeurs de la matrice rmat6
c     mat4= pointeurs des termes localises sur la diagonale de la matrice
c
c indices de fonctionnement divers:
c
c     initb0: 1 si une valeur initiale est donnee dans x0, 0 sinon (initialisation a 0)
c     niveau: niveau de preconditionnement propose
c             en retour, le niveau effectivement utilise
c     eps (DP):  tolerance sur la convergence du residu
c     itrait (entier) :
c            1:  resolution complete
c            > 1:  pas de calcul des pointeurs de preconditionnement
c                  (si le niveau n'a pas change)
c            > 2:  pas de calcul de la matrice de precontitionnement
c                  (si la matrice n'a pas ou peu change)
c     istgrc:  1 si on doit s'arreter en cas de non convergence (obligatoire)
c     niter: nombre maximu d(iterations (en retour, le nombre reel)
c     i_cal_pc: 1 pour utiliser le non calcul eventuel de la matrice de preconditionnement
c               0 pour forcer le calcul dans tous les cas (1 conseille)
c     message: code d'erreur en retour pour les sous-programmes qui sauraient
c              l'utiliser:
c             message = 0 en entree: la non convergence prrovoque l'arret
c             message = 1 en entree: si message = 1 en retour, tout va bien
c                                    si message = 2 en retour, non convergence
c     ieco:   si ieco=1, on utilise une version simple precision de la matrice de
c             preconditionnement pour economiser de la place
c             (reserve aux experts. ieco = 0 conseille)
c               
c     
      dimension b(*),x(*),x0(*)
        dimension temps(2),temtot(2)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
        common /entsor/ lec,imp
c     real*4 rmac6(*),rmat6(*)
      dimension rmat6(*)

!
! si les pointeurs sont declares save, ils sont statiques, donc partages
! dans le cas //, ce qui cree des conflits

!     integer(4),pointer,save :: mac5(:)=>null(),mac7(:)=>null()
!     integer(4),pointer,save :: mac4(:)=>null()
!     real(8),pointer,save :: rmac6(:)=>null()
!     real(4),pointer,save :: rmac6sp(:)=>null()
!
! meme probleme si on les initialise: une variable initialisee est
! forcement statique

!     integer(4),pointer :: mac5(:)=>null(),mac7(:)=>null()
!     integer(4),pointer :: mac4(:)=>null()
!     real(8),pointer :: rmac6(:)=>null()
!     real(4),pointer :: rmac6sp(:)=>null()
      integer(4),pointer :: mac5(:),mac7(:)
      integer(4),pointer :: mac4(:)
      real(8),pointer :: rmac6(:)
      real(4),pointer :: rmac6sp(:)
!
! tableaux de travail.
!
      real(8),allocatable :: p1(:),p2(:),ap(:),atp(:),z(:),r1(:),r2(:),
     1       xcop(:),xsav(:)
!     integer(4),allocatable,save :: mac4(:), mac7(:)
      integer deg_max_gc

!
! tableau des longueurs de mac5 en fonction du niveau
! ce tableau est construit au fur et a mesure des essais de resolution

        integer tlmac5(0:10,max_appli)
        !data tlmac5/(max_appli*11)*0/
        save tlmac5
        logical,save :: b_first_pass

	character(20) nomfic
!
! compteur de solutions sauvegardees

	integer(4),save :: num_save
	data num_save/1/

creal*4 rmac6

      interface
         subroutine calplu (ntdl,niveau,long,mat4,mat5,
     1                  mac4,mac5,mac7,tlmac5,iappli)
	integer(4) :: ntdl,niveau,long,mat4(*),mat5(*),
     1	              mac4(*),mac7(*),tlmac5(0:10,*),iappli
	integer(4),pointer :: mac5(:)
	end subroutine calplu
      end interface
  
      imprim = imp

! verification de depassement de iappli

      if (iappli .gt. max_appli) then
         write (imp,*) ' Dimension de max_appli insuffisante'
         write (imp,*) ' Valeur actuelle:',max_appli
         call sortie ('solgc')
      endif

      if (idump_solgc .gt. 2) then
         write (imp,*)
         write (imp,*) ' resolution par gradient conjugue:'
         write (imp,*) ' solgc, niveau =',niveau,' itrait =',itrait
         write (imp,*) ' solgc, niter =',niter
         write (imp,*) '        initb0 =',initb0
         write (imp,*) '        long =',long
         write (imp,*) '        ntdl =',ntdl
         write (imp,*) ' systeme dans solgc'
         call impsmo (mat4,mat5,rmat6,b,ntdl)
         write (imp,*) ' solution initiale'
         write (imp,*) (i,x(i),i=1,ntdl)
      endif
ccall sortie ('solgc')
c      call dtime (temps)
      temtot(1) = 0.
      temtot(2) = 0.
      temps(1) = 0.
      temps(2) = 0.

!
! initialisation tlmac5
      if (b_first_pass) then
         tlmac5(:,:) = 0
         b_first_pass = .FALSE.
      endif

!
! pointeurs initialises a zero, sinon ils risquent de ne pas etre
! initialises

      nullify (mac5)
      nullify (mac7)
      nullify (mac4)
      nullify (rmac6)
      nullify (rmac6sp)
!
! dimension de mac5 pour le niveau 0
!
      tlmac5(0,iappli) = mat4(ntdl+1)
!
! allocation des tableaux de travail
!
c$OMP PARALLEL DEFAULT (private)

!        write (imp,*) ' Numero de thread ',omp_get_thread_num()

	allocate (p1(ntdl),stat=ires)
	call diag_alloc (ires,' p1 (solgc)')
	allocate (p2(ntdl),stat=ires)
	call diag_alloc (ires,' p2 (solgc)')
	allocate (ap(ntdl),stat=ires)
	call diag_alloc (ires,' ap (solgc)')
	allocate (atp(ntdl),stat=ires)
	call diag_alloc (ires,' atp (solgc)')
	allocate (z(ntdl),stat=ires)
	call diag_alloc (ires,' z (solgc)')
	allocate (r1(ntdl),stat=ires)
	call diag_alloc (ires,' r1 (solgc)')
	allocate (r2(ntdl),stat=ires)
	call diag_alloc (ires,' r2 (solgc)')
	allocate (xcop(ntdl),stat=ires)
	call diag_alloc (ires,' xcop (solgc)')
	allocate (xsav(ntdl),stat=ires)
	call diag_alloc (ires,' xsav (solgc)')
	if (.not. associated (mac4)) then
	   allocate (mac4(ntdl+1),stat=ires)
	   call diag_alloc (ires,' mac4 (solgc)')
	endif
	if (.not. associated (mac7)) then
	   allocate (mac7(ntdl+1),stat=ires)
	   call diag_alloc (ires,' mac7 (solgc)')
	endif

	call coptr (x,xsav,ntdl)

c
c point de reprise en cas de non convergence
10000 continue
!      write (imp,*) ' solgc, niveau =',niveau,' itrait =',itrait
!      write (imp,*) ' solgc, ieco =',ieco
c
c ---> itrait > 1: on ne calcule pas les pointeurs de la matrice
c                  de preconditionnement
c
      if (itrait.le.1) then
!
! si on a deja fait le calcul, on relit
         if (b_sto_mac5(niveau,iappli) .eqv. .TRUE.) then
            call lecmac5 (mac4,mac5,mac7,niveau,iappli)
	 else
!
! sinon, on calcule et on stocke
	 longa = long
         call CALPLU (NTDL,NIVEAU,LONG,MAT4,MAT5,
     2                MAC4,MAC5,MAC7,tlmac5,iappli)

         ! PG 15/03/15: ce stockage fait bugger dans libaq
         ! PG 27/03/15: stockage avec l'indice d'application
         call stomac5 (mac4,mac5,mac7,ntdl,niveau,iappli)

	 if (longa .ne. long) then
c
c sortie de solgc avec un indice d'erreur
! 3/8/06: normalement, on a fait ce qu'il faut. On continue
!    niter = -1
!    return
	endif
	endif
!
! enregistrement de la taille de mac5

!     write (imp,*) ' taille de mac5 pour le niveau',niveau,long

	   tlmac5(niveau,iappli) = long
	endif
call dtime (temps)
cwrite (imp,*) ' temps dans calplu',temps
      temtot(1) = temtot(1)+temps(1)
      temtot(2) = temtot(2)+temps(2)
      sigma = 1.
!
! allocation de rmac6
!
      lmac5 = mac4(ntdl+1)
      if (idump_solgc .gt. 1)
     1   write (imp,*) ' Allocation de mac6, dimension',lmac5
      if (ieco .eq. 0) then
         if (associated (rmac6)) then
	    if (idump_solgc .gt. 1)
     1      write (imp,*) ' desallocation de mac6'
	    deallocate (rmac6)
         endif
         allocate (rmac6(lmac5),stat=ires)
!
! allocation minimale pour rmac6sp
         if (associated (rmac6sp)) deallocate (rmac6sp)
         allocate (rmac6sp(1),stat=ires)
      else
         if (associated (rmac6sp)) then
	    if (idump_solgc .gt. 1)
     1      write (imp,*) ' desallocation de mac6'
	    deallocate (rmac6sp)
         endif
         allocate (rmac6sp(lmac5),stat=ires)
!
! allocation minimale pour rmac6
         if (associated (rmac6)) deallocate (rmac6)
         allocate (rmac6(1),stat=ires)
      endif
      call diag_alloc (ires,' rmac6 (solgc)')
c
c ---> itrait > 2: on ne calcule pas la matrice de preconditionnement
c
      if (itrait.le.2) then
	 if ((b_sauve_mac6 .eqv. .TRUE.)
     1   .and. (b_sto_mac6(niveau,iappli_mac6) .eqv. .TRUE.)) then

! relecture
            call lecmac6 (rmac6,niveau)
	 else
	 if (ieco .eq. 1) then
         call CDLU1R(NTDL,SIGMA,MAT4,MAT5,rmat6,
     S                             MAC4,MAC5,rmac6sp,MAC7)
	else
         call CDLU1Rdp(NTDL,SIGMA,MAT4,MAT5,rmat6,
     S                             MAC4,MAC5,rmac6,MAC7)
	endif
	 ! PG 15/03/15: ce stockage fait bugger dans libaq
         ! if ((b_sauve_mac6 .eqv. .TRUE.)
!    1   .and. (b_sto_mac6(niveau,iappli_mac6) .eqv. .FALSE.)) then
!        call stomac6 (rmac6,mac4(ntdl+1),niveau)
!b_sto_mac6(niveau,iappli_mac6) = .TRUE.
!endif
	endif
	endif
c      call dtime (temps)
cwrite (imp,*) ' temps dans cdlu1r',temps
      temtot(1) = temtot(1)+temps(1)
      temtot(2) = temtot(2)+temps(2)
c     call CDLU2R(NTDL,MAC4,MAC5,rmac6,MAC7)
c
C
C     APPEL DE L ALGORITHME
C     ---------------------
C
      CALL CAPLU2(NTDL,mac4,mac5,mac7)
c      call dtime (temps)
cwrite (imp,*) ' temps dans caplu2',temps
      temtot(1) = temtot(1)+temps(1)
      temtot(2) = temtot(2)+temps(2)
      nitloc = niter
	if (ieco .eq. 1) then
      CALL DGRA1R(NTDL,ndsm,eps,initb0,
     &   mat4,mat5,rmat6,x0,b,
     &   mac4,mac5,rmac6sp,mac7,
     &   r1,r2,p1,p2,ap,atp,z,x,xcop,istgrc,nitloc)
	else
      CALL DGRA1Rdp(NTDL,ndsm,eps,initb0,
     &   mat4,mat5,rmat6,x0,b,
     &   mac4,mac5,rmac6,mac7,
     &   r1,r2,p1,p2,ap,atp,z,x,xcop,istgrc,nitloc)
	endif
cWrite (imp,*) ' nitloc apres dgra1rdp',nitloc 
c
c si non convergence, on augmente le niveau de preconditionnement
c et on ressaie
      if (nitloc.eq.999) then
!
! stockage du resultat pour reprise eventuelle
!
!if (num_save .lt. 10) write (unit=nomfic,fmt="('last_sol',i1)"),num_save
!if (num_save .ge. 10) write (unit=nomfic,fmt="('last_sol',i2)"),num_save
!iul = 1 !50
!call connecte (nomfic,iul,iopen,"nfor")
!write (imp,*) ' Sauvegarde de la derniere solution dans ',nomfic
!write (iul) (x(i),i=1,ntdl)
!close (iul)
!       call free_ul (iul)
!num_save = num_save + 1
c
c on essaie d'abord de recalculer la matrice de preconditionnement
c sans changer de niveau, si on ne l'a pas encore fait
c (cette option n'est active que si i_cal_pc = 0)

          if (itrait .eq.3 .and. i_cal_pc .eq. 0) then
	     itrait = 2
             write (imp,54)
	  else
	     niveau = niveau + 1
	Write (imp,*) ' augmentation de niveau',niveau
             if (niveau .gt. deg_max_gc) then
                write (imp,'(///a//)')
     1          ' Convergence du gradient conjugue impossible'
                if (idump_solgc .gt. 1)
     1             call impsmo (mat4,mat5,rmat6,b,ntdl)
	        if (message .eq.0) call sortie ('solgc')
c
c message pour les ss-pgm qui savent traiter l'erreur
	        message = 2
			return
	        !return    ! on sort plus loin par un test sur le niveau
	        !(pour sortir proprement du bloc openMP)
             endif
	     itrait = 1
             write (imp,53) niveau, mac4(ntdl+1)
!
! reinitialisation matrice mac6 au prochain tour
	     call init_sauve_mac6 (iap = iappli_mac6)
	  endif
	  call coptr (xsav,x,ntdl)
          !if (niveau .le. deg_max_gc) goto 10000
		  goto 10000
      endif
      niter = nitloc
c      call dtime (temps)
cwrite (imp,*) ' temps dans dgra1r',temps
      temtot(1) = temtot(1)+temps(1)
      temtot(2) = temtot(2)+temps(2)

! on ne desalloue pas mac5, mac4 et mac7, car on les reutilise si itrait > 1
! modif le 19/06/18:
! on desalloue tout parce que cela cree des pb en mode parallele
!
      if (associated(mac4)) then        ! 19/06/18
         deallocate (mac4)              ! 19/06/18
         nullify(mac4)                  ! 19/06/18
      endif                             ! 19/06/18
      if (associated(mac5)) then        ! 19/06/18
         deallocate (mac5)              ! 19/06/18
         nullify(mac5)                  ! 19/06/18
      endif                             ! 19/06/18
      if (associated(rmac6)) then       ! 19/06/18
         deallocate (rmac6)             ! 19/06/18
         nullify(rmac6)                 ! 19/06/18
      endif                             ! 19/06/18
      if (associated(rmac6sp)) then     ! 19/06/18
         deallocate (rmac6sp)           ! 19/06/18
         nullify(rmac6sp)               ! 19/06/18
      endif                             ! 19/06/18
      if (associated(mac7)) then        ! 20/06/18
         deallocate (mac7)              ! 20/06/18
         nullify(mac7)                  ! 20/06/18
      endif                             ! 20/06/18
	deallocate (p1)
	deallocate (p2)
	deallocate (ap)
	deallocate (atp)
	deallocate (z)
	deallocate (r1)
	deallocate (r2)
	deallocate (xcop)
	deallocate (xsav)

	!deallocate (mac4) ! 27/03/15
	!deallocate (mac7) ! 27/03/15
cwrite (imp,*) ' temps total dans solgc',temtot
ccall dtime (temps)
cwrite (imp,*) ' temps dans grad',temps
53    format (' Reprise de la resolution avec un nouveau niveau de',
     1' preconditionnement (solgc)',i4,' lmac5 =',i10)
54    format (' Reprise de la resolution avec une matrice de',
     1' preconditionnement remise a jour (solgc)')
      end
c
      SUBROUTINE CAPLU2(NTDL,MAC4,MAC5,MAC7)
        include 'dprec.h'
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                     S.P. CAPLU2
C                     -----------
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CALCUL LE POINTEUR MAC7 ADRESSE DANS CA DU DERNIER
C ---   COEFFICIENT DE LA PARTIE TRIANGULAIRE INFERIEURE
C
C PARAMETRES D ENTREE :
C ---------------------
C MAC4,MAC5 : LES POINTEURS ASSOCIES
C
C PARAMETRE DE SORTIE :
C --------------------
C MAC7      : LE POINTEUR SUR LE DERNIER COEFFICIENT DE L
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION MAC4(*),MAC7(NTDL),MAC5(*)
C

cwrite (imp,*) ' caplu2...'

      K1=1
      DO 1 I=1,NTDL
      K2=MAC4(I+1)
      MAC7(I)=K1-1
      DO 2 K=K1,K2
      IF(MAC5(K).LT.I) THEN
      MAC7(I)=K
      ELSE
      GO TO 3
      END IF
2     CONTINUE
3     K1=K2+1
1     CONTINUE
C
      RETURN
      END
      SUBROUTINE CDLU1R(NTDL,SIGMA,MAT4,MAT5,A,
     S                             MAC4,MAC5,CA,MAC7)
        include 'dprec.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP CALCULE LA  MATRICE DE CONDITIONNEMENT
C ----  ASSOCIEE A LA FACTORISATION DE GAUSS INCOMPLETE
C       DE A SUIVANT LA NOTION DE NIVEAU
C
C       VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   NTDL      : LE RANG DU SYSTEME
C   SIGMA     : PARAMETRE DE FACTORISATION
C   MAT4,MAT5 : LES POINTEURS ASSOCIES
C   A          : LA MATRICE DU SYSTEME
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   MAC4,MAC5,MAC7 : LES POINTEURS ASSOCIES
C   CA             : LA MATRICE DE CONDITIONNEMENT
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION A(*),CA(*)
      DIMENSION MAT4(*),MAT5(*),MAC4(*),MAC5(*)
      DIMENSION MAC7(NTDL)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON/TRAVA1/NX(29),IMPRE,IFILL(3)

	integer(4),allocatable :: ind(:)
	real(8),allocatable :: cx(:)
	real*4 ca

        allocate(ind(ntdl),stat=ires)
        call diag_alloc (ires,' ind (cdlu1r)')
        allocate(cx(ntdl),stat=ires)
        call diag_alloc (ires,' cx (cdlu1r)')
C
C     VALEURS SEUIL
C     -------------
C

cwrite (imp,*) ' cdlu1r...'

      IMPR1=IABS(IMPRE)
c     SEUIL=1.D-06
      seuil=1.d-20
      IPIV=0
      CXI=abspg (A(MAT4(2)))
C
C     CALCUL DE LA MATRICE DE CONDITIONNEMENT
C     ---------------------------------------
C
      KC1=1
      K1=1
      DO 1 I=1,NTDL
      KC2=MAC4(I+1)
C     INITIALISATION DE IND ET CX
      DO 2 K=KC1,KC2
      J=MAC5(K)
C     NG 22/06/2023 : Print voisins et pos mat4, mat5      
C     WRITE(*,*) I,J,K,KC1,KC2
      IND(J)=I
      CX(J)=0.D0
2     CONTINUE
C     INITIALISATION DE CX PAR A
      K2=MAT4(I+1)
      DO 3 K=K1,K2
      J=MAT5(K)
      CX(J)=A(K)
3     CONTINUE
      CX(I)=CX(I)*SIGMA
C     BOUCLE D'ELIMINATION DE LA PARTIE GAUCHE DE LA DIAGONALE
      DO 4 K=KC1,MAC7(I)
      J=MAC5(K)
      CXJ=CX(J)*CA(MAC4(J+1))
      DO 5 L=MAC7(J)+1,MAC4(J+1)-1
      JJ=MAC5(L)
      IF(IND(JJ).EQ.I) CX(JJ)=CX(JJ)-CXJ*CA(L)
5     CONTINUE
      CX(J)=CXJ
4     CONTINUE
C     STOCKAGE DE CX DANS CA
      DO 6 K=KC1,KC2-1
      J=MAC5(K)
      CA(K)=CX(J)
6     CONTINUE
      IF(abspg (CX(I)/CXI).LT.SEUIL) THEN
                    IPIV=IPIV+1
                    CX(I)=CXI
                                   ELSE
          IF(abspg (CX(I)).LT.SEUIL) THEN
                    CXI=CX(I)
          END IF
      END IF
      CA(KC2)=1.D0/CX(I)
      KC1=KC2+1
      K1=K2+1
1     CONTINUE
C
C     IMPRESSIONS
C     -----------
C
      IF(IMPR1.GE.2) THEN
      IF(IPIV.GT.0) WRITE(IMPRIM,1000) IPIV,NTDL
      END IF
C
      deallocate (ind)
      deallocate (cx)

      RETURN
1000  FORMAT(I5,' COEFFICIENTS DIAGONAUX SUR',I5,' SONT NULS')
      END
      SUBROUTINE CDLU1Rdp(NTDL,SIGMA,MAT4,MAT5,A,
     S                             MAC4,MAC5,CA,MAC7)
        include 'dprec.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP CALCULE LA  MATRICE DE CONDITIONNEMENT
C ----  ASSOCIEE A LA FACTORISATION DE GAUSS INCOMPLETE
C       DE A SUIVANT LA NOTION DE NIVEAU
C
C       VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   NTDL      : LE RANG DU SYSTEME
C   SIGMA     : PARAMETRE DE FACTORISATION
C   MAT4,MAT5 : LES POINTEURS ASSOCIES
C   A          : LA MATRICE DU SYSTEME
C   CX,IND    : TABLEAUX DE TRAVAIL
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   MAC4,MAC5,MAC7 : LES POINTEURS ASSOCIES
C   CA             : LA MATRICE DE CONDITIONNEMENT
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION A(*),CA(*)
      DIMENSION MAT4(*),MAT5(*),MAC4(*),MAC5(*)
      DIMENSION MAC7(NTDL)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON/TRAVA1/NX(29),IMPRE,IFILL(3)

	integer(4),allocatable :: ind(:)
	real(8),allocatable :: cx(:)
	real*8 ca

        allocate(ind(ntdl),stat=ires)
        call diag_alloc (ires,' ind (cdlu1rdp)')
        allocate(cx(ntdl),stat=ires)
        call diag_alloc (ires,' cx (cdlu1rdp)')
C
C     VALEURS SEUIL
C     -------------
C

cwrite (imp,*) ' cdlu1r...'

      IMPR1=IABS(IMPRE)
c     SEUIL=1.D-06
      seuil=1.d-20
      IPIV=0
      CXI=abspg (A(MAT4(2)))
C
C     CALCUL DE LA MATRICE DE CONDITIONNEMENT
C     ---------------------------------------
C
      KC1=1
      K1=1
      DO 1 I=1,NTDL
      KC2=MAC4(I+1)
C     INITIALISATION DE IND ET CX
      DO 2 K=KC1,KC2
      J=MAC5(K)
      IND(J)=I
      CX(J)=0.D0
2     CONTINUE
C     INITIALISATION DE CX PAR A
      K2=MAT4(I+1)
      DO 3 K=K1,K2
      J=MAT5(K)
      CX(J)=A(K)
3     CONTINUE
      CX(I)=CX(I)*SIGMA
C     BOUCLE D'ELIMINATION DE LA PARTIE GAUCHE DE LA DIAGONALE
      DO 4 K=KC1,MAC7(I)
      J=MAC5(K)
      CXJ=CX(J)*CA(MAC4(J+1))
      DO 5 L=MAC7(J)+1,MAC4(J+1)-1
      JJ=MAC5(L)
      IF(IND(JJ).EQ.I) CX(JJ)=CX(JJ)-CXJ*CA(L)
5     CONTINUE
      CX(J)=CXJ
4     CONTINUE
C     STOCKAGE DE CX DANS CA
      DO 6 K=KC1,KC2-1
      J=MAC5(K)
      CA(K)=CX(J)
6     CONTINUE
      IF(abspg (CX(I)/CXI).LT.SEUIL) THEN
                    IPIV=IPIV+1
                    CX(I)=CXI
                                   ELSE
          IF(abspg (CX(I)).LT.SEUIL) THEN
                    CXI=CX(I)
          END IF
      END IF
      CA(KC2)=1.D0/CX(I)
      KC1=KC2+1
      K1=K2+1
1     CONTINUE
C
C     IMPRESSIONS
C     -----------
C
      IF(IMPR1.GE.2) THEN
      IF(IPIV.GT.0) WRITE(IMPRIM,1000) IPIV,NTDL
      END IF
C
      deallocate (ind)
      deallocate (cx)
      RETURN
1000  FORMAT(I5,' COEFFICIENTS DIAGONAUX SUR',I5,' SONT NULS')
      END
      SUBROUTINE CDLU2R(NTDL,MAC4,MAC5,CA,MAC7)
        include 'dprec.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : COMPRESSION DE LA MATRICE DE PRECONDITIONNEMENT
C ---
C       VERSION SIMPLE PRECISION
C
C PARAMETRES D ENTREE :
C -------------------
C NTDL   : NOMBRE DE DEGRES DE LIBERTE DU SYSTEME
C MAC4   : POINTEUR SUR LES LIGNES DE LA MATRICE
C MAC5   : POINTEUR SUR LES COLONNES DE LA MATRICE
C CA     : MATRICE DE CONDITIONNEMENT A COMPRESSER
C MAC7   : POINTEUR SUR LA PARTIE INFERIEURE DE LA MATRICE
C
C PARAMETRES RESULTATS :
C --------------------
C MAC4   : POINTEUR SUR LES LIGNES DE LA MATRICE COMPRESSEE
C MAC5   : POINTEUR SUR LES COLONNES DE LA MATRICE COMPRESSEE
C CA     : MATRICE DE PRECONDITIONNEMENT COMPRESSEE
C MAC7   : POINTEUR SUR LA PARTIE INFERIEURE DE LA MATRICE
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      INTEGER MAC4(*),MAC5(*),MAC7(NTDL)
      DIMENSION CA(*)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON/TRAVA1/NX(29),IMPRE,IFILL(3)
C

cwrite (imp,*) ' cdlu2r...'

      IMPR1=IABS(IMPRE)
c     EPS=1.D-06
      eps=1.d-12
      LA5=MAC4(NTDL+1)
      L5=0
      K1=1
      DO 1 I=1,NTDL
      K2=MAC4(I+1)
      DO 2 K=K1,K2
      IF(ABS(CA(K)*CA(K2)).LT.EPS) GO TO 2
      L5=L5+1
      MAC5(L5)=MAC5(K)
      CA(L5)=CA(K)
2     CONTINUE
      MAC4(I+1)=L5
      K1=K2+1
1     CONTINUE
      CALL CAPLU2(NTDL,MAC4,MAC5,MAC7)
C
C     IMPRESSIONS
C     -----------
C
      IF(IMPR1.GE.2) THEN
      WRITE(IMPRIM,1000) LA5,L5
      END IF
C
      RETURN
1000  FORMAT(' COMPRESSION DE LA MATRICE DE PRECONDITIONNEMENT',/,
     S       ' NOMBRE DE COEFFICIENTS AVANT COMPRESSION',I7/,
     S       ' NOMBRE DE COEFFICIENTS APRES COMPRESSION',I7)
      END
      SUBROUTINE AMATBR(NCODSA,NTDL,MAT4,MAT5,A,X,Y)
        include 'dprec.h'
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                     S.P. AMATBR
C                     -----------
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : FAIRE LE PRODUIT D UNE MATRICE A OU SEULS LES NON ZERO SONT
C ----- STOCKES (S.D. AMAT) PAR UN VECTEUR X
C       VERSION REELLE SIMPLE PRECISION
C
C PARAMETRES D ENTREE :
C ---------------------
C NCODSA : MODE DE STOCKAGE DE A 0 DIAGONALE
C                                1 SYMETRIQUE
C                               -1 NON SYMETRIQUE
C NTDL   : ORDRE DE LA MATRICE A
C MAT4   : MAT4(1)=0 MAT4(I+1)=ADRESSE DU COEFFICIENT DIAGONAL DE LA
C          LIGNE I DANS LES TABLEAUX MAT5 ET A
C MAT5   : MAT5(K)=NO DE LA COLONNE DU COEFFICIENT A(K)
C A      : COEFFICIENTS DE LA MATRICE RANGES PAR LIGNE
C          LE COEFFICIENT DIAGONAL EN DERNIER
C X      : VECTEUR(NTDL) A MULTIPLIER PAR A
C
C PARAMETRE RESULTAT :
C --------------------
C Y      : VECTEUR A * X
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C PROGRAMMEUR : MARINA VIDRASCU INRIA 1988
C ......................................................................
      DIMENSION A(*),MAT4(*),MAT5(*),X(NTDL),Y(NTDL)
C
      IF(NCODSA .NE. 0) THEN
C
C        INITIALISATION A ZERO DE Y
C        --------------------------
C
         DO 1 I=1,NTDL
            Y(I) = 0.
    1    CONTINUE
      END IF
C
C     AIGUILLAGE SELON LE TYPE DE LA MATRICE A
C     ----------------------------------------
C
      IF(NCODSA .GT. 0) THEN
C
C        MATRICE SYMETRIQUE
C        ------------------
C
          DO 2 I = 1,NTDL
             N    = MAT4(I+1)-MAT4(I)
             Y(I) = SPDOT(N,X,MAT5(MAT4(I)+1),A(MAT4(I)+1))
             CALL SPAXPY(N-1,X(I), A(MAT4(I)+1),Y,MAT5(MAT4(I)+1))
   2      CONTINUE
      ELSE IF (NCODSA .EQ. 0) THEN
C
C        MATRICE DIAGONALE
C        -----------------
C
           DO 201 I=1,NTDL
               Y(I) = X(I) * A(I)
  201       CONTINUE
      ELSE IF (NCODSA .LT. 0) THEN
C
C        MATRICE NON SYMETRIQUE
C        ----------------------
C
         DO 101 I = 1,NTDL
             N    = MAT4(I+1)-MAT4(I)
             Y(I) = SPDOT(N,X,MAT5(MAT4(I)+1),A(MAT4(I)+1))
  101    CONTINUE
      END IF
      END
      SUBROUTINE CL2VER(LOM,ALPHA1,B01,ALPHA2,B02,B)
        include 'dprec.h'
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C  BUT : B <= ALPHA1 * B01 + ALPHA2 * B02
C  ----- B , B01 , B02 VECTEURS DE LOM VARIABLES
C
C   PARAMATRES D ENTREE:
C   --------------------
C   LOM NBRE DE VARIABLES DES TABLEAUX B,B01,B02
C   ALPHA1,ALPHA2,B01,B02 COMME INDIQUE CI-DESSUS
C
C   PARAMETRE DE SORTIE:
C   --------------------
C   B  TABLEAU DU RESULTAT DE LA COMBINAISON LINEAIRE
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : A.PERRONNET LAN189 PARIS ET IRIA  NOVEMBRE 1979
C ......................................................................
      DIMENSION B01(LOM),B02(LOM),B(LOM)
C
C     TEST SUIVANT LES VALEURS DE ALPHA1 , ALPHA2
C
      IF(ALPHA1.EQ.1.D0) GOTO 1
      IF(ALPHA2.EQ.1.D0) GOTO 2
         DO 3 I=1,LOM
         B(I)=ALPHA1*B01(I)+ALPHA2*B02(I)
    3    CONTINUE
      GOTO 100
C
C     ALPHA1=1.D0
C
    1  IF(ALPHA2.EQ.1.D0) GOTO 4
         DO 5 I=1,LOM
         B(I)=B01(I)+ALPHA2*B02(I)
    5    CONTINUE
      GOTO 100
C
C     ALPHA1=1.D0,ALPHA2=1.D0
C
    4    DO 6 I=1,LOM
         B(I)=B01(I)+B02(I)
    6    CONTINUE
      GOTO 100
C
C     ALPHA1 NON NUL , ALPHA2=1.D0
C
    2    DO 7 I=1,LOM
         B(I)=ALPHA1*B01(I)+B02(I)
    7    CONTINUE
C
  100 RETURN
      END
      SUBROUTINE  DGRA1R(NTDL,NDSM,EPS,INITB0,
     &                   MAT4,MAT5,A,B0,B,MAC4,MAC5,CA,MAC7,
     &                   R1,R2,P1,P2,AP,ATP,Z,X,xcop,istgrc,k)
        include 'dprec.h'
        include 'dump.h'
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   BUT : ITERATIONS DE DOUBLE GRADIENT CONJUGUE ACCELEREES
C   ----- AVEC PRECONDITIONNEMENT
C
C          VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   --------------------
C   NTDL   : ORDRE DE LA MATRICE A
C   NDSM   : NBRE DE SECONDS MEMBRES
C   EPS    : PARAMETRE D ARRET DES ITERATIONS DU GRADIENT CONJUGUE
C   INITB0 : 0 INITIALISATION  PAR ZERO
C            1 INITIALISATION  PAR LE TABLEAU B0
C   MAT4   : POINTEUR AMAT4 SUR LES COEFFICIENTS DIAGONAUX
C   MAT5   : POINTEUR AMAT5 SUR LES COLONNES
C   A      : MATRICE DU SYSTEME LINEAIRE
C   B0     : TABLEAU DES VALEURS INITIALES DU G.C.
C   B      : SECOND MEMBRE
C   MAC4   : POINTEUR AMAC4 SUR LES COEFFICIENTS DIAGONAUX
C   MAC5   : POINTEUR AMAC5 SUR LES COLONNES
C   CA     : MATRICE DE PRECONDITIONNEMENT
C   MAC7   : POINTEUR SUR L ET U DANS CA
C   R1     : VECTEUR AUXILIAIRE
C   R2     : VECTEUR AUXILIAIRE
C   P1     : VECTEUR AUXILIAIRE
C   P2     : VECTEUR AUXILIAIRE
C   AP     : VECTEUR AUXILIAIRE
C   ATP    : VECTEUR AUXILIAIRE
C   Z      : VECTEUR AUXILIAIRE
C   X      : VECTEUR AUXILIAIRE
C
C   PARAMETRES DE SORTIE:
C   ---------------------
C   B      : LE TABLEAU B EST ECRASE PAR LES SOLUTIONS
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c        save ipass
c     DIMENSION A(*),B(NDSM,*),B0(NDSM,*),R1(*),R2(*),P1(*),P2(*)
      DIMENSION A(*),B(NTDL,*),B0(NTDL,*),R1(*),R2(*),P1(*),P2(*)
     &,AP(*),ATP(*),Z(*),X(*),CA(*)
      DIMENSION MAT4(*),MAT5(*),MAC4(*),MAC5(*),MAC7(*),xcop(*)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON /TRAVA1/NX(29),IMPRE,IFILL(3)

	real*4 ca

1000  FORMAT(' nb d iterations du',i5,'-eme systeme=',i7,
     S ' norme du residu',g13.6)
1001  FORMAT(' ERREUR DGRA1R : NON CONVERGENCE APRES',I8,' ITERATIONS')
1002  FORMAT(' SYSTEME NO',I8/1X,130('-')/
     &,' RKNORM=',G13.6,' ITEMAX=',I5)
1003  FORMAT(' SOLUTION DU ',I5,'-EME SYSTEME'/1X,130('-')/
     &5(' X(',I5,')=',G14.7))
1004  FORMAT(5(' X(',I5,')=',G14.7))
1005  FORMAT(' iteration',i5,' norme l2 residu=',
     &g13.6,' eps1 =',g13.6)
1006  FORMAT('       nb d iterations de G.Conj. ',i5,
     S ' norme du residu',g13.6)
c        data ipass/1/
C
C     INITIALISATIONS DES CONSTANTES
C     ------------------------------
C
c     write (imp,'(10i5)') (mat4(i),i=1,ntdl+1)
c     write (imp,'(10i5)') (mat5(i),i=1,mat4(ntdl+1))
c     write (imp,'(8(e11.4,1x))') (a(i),i=1,mat4(ntdl+1))
c     write (imp,'(8(e11.4,1x))') (b(i,1),i=1,ntdl)
ccall sortie ('STOP DGRA1R')

cwrite (imp,*) ' dgra1r...'

      IMPR1  = IABS(IMPRE)
        un = 1.
cif (ipass.eq.1) then
c     ITEMAX = NTDL / 5
c     itemax = ntdl
cipass = 2
celse
citemax = 20
cendif
        itemax = k
      NCODSA = -1
C
C    BOUCLE SUR LES NDSM SYSTEMES A RESOUDRE
C    ---------------------------------------
C
       DO 11 J=1,NDSM
C
C    INITIALISATIONS
C    ---------------
C
   13    DO 9 I=1,NTDL
c        Z(I)= B(J,I)
         Z(I)= B(I,J)
         IF(INITB0.NE.0) GOTO 7
         X(I) = 0.
         GOTO 9
c   7    X(I) = B0(J,I)
    7    X(I) = B0(I,J)
    9    CONTINUE
C
C     (R) = (B) - ((A)) * (B0)
C     (G) = (B) - ((A)) * (B0)
C     (U) = (B) - ((A)) * (B0)
C     ------------------------
C
         call maxtab (x,ntdl,rmax)
         eps1 = 0.
         if (rmax.ne.0.) then
c
c calcul de l'objectif par une perturbation de la solution:
c on injecte dans l'equation une solution petite devant la solution
c initiale
            call initr (xcop, ntdl, rmax * eps)
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,xcop,AP)
            CALL DGRA2R(NTDL,MAC4,MAC5,CA,MAC7,AP,R1)
            RKNORM=PROSCR(R1,R1,NTDL)
            eps1 = rknorm
	 else
c
c si le second membre est nul egalement, on a termine
	    call maxtab (b(1,j),ntdl,smmax)
	    if (smmax .eq. 0.) then
	       write (imprim,*) ' Systeme trivialement nul'
               k = 1
	       goto 11
	    endif
         endif
         CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,X,AP)
         CALL CL2VER(NTDL, un,Z, -un,AP,ATP)
         CALL DGRA2R(NTDL,MAC4,MAC5,CA,MAC7,ATP,R1)
         DO 30 I=1,NTDL
         R2(I)=R1(I)
         P1(I)=R1(I)
         P2(I)=R1(I)
   30    CONTINUE
C
         RKNORM=PROSCR(R1,R1,NTDL)
         R0R=RKNORM
C
         K = 0
         if (eps1.eq.0.) EPS1 = EPS * EPS * RKNORM

        epslas = 1.d30
         IF(IMPR1.GE.3) WRITE (IMPRIM,1002) J,RKNORM,ITEMAX
c        write (imprim,1002) j,rknorm,itemax
         IF(RKNORM .NE. 0.) GOTO 8
         IF(INITB0 .EQ. 0 ) GOTO 10
         INITB0 = 0
         GOTO 13
C
C      ITERATIONS DE L'ALGORITHME
C      --------------------------
C
    8       K = K + 1
C
C           ALPHAK  = ( R0 , R ) / ( R0 , (A) * G )
C           ---------------------------------------
C
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,R2,ATP)
            CALL DGRA2R(NTDL,MAC4,MAC5,CA,MAC7,ATP,AP)
            R0AG = PROSCR(R1,AP,NTDL)
            ALPHAK = R0R / R0AG
C
C           (H) =  (U) -  ALPHAK * ((A)) * (G)
C           (X) =  (X) +  ALPHAK * ( U + H )
C           ----------------------------------
C
            DO 21 I=1,NTDL
            AP(I)=P1(I)-ALPHAK*AP(I)
            ATP(I)=P1(I)+AP(I)
   21       X(I)=X(I)+ALPHAK*ATP(I)
C
C           (R) = (R) - ALPHAK * ((A)) * ( U + H )
C           --------------------------------------
C
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,ATP,Z)
            CALL DGRA2R(NTDL,MAC4,MAC5,CA,MAC7,Z,ATP)
            DO 22 I=1,NTDL
   22       P2(I)=P2(I)-ALPHAK*ATP(I)
C
C           RKNORME = ( R , R )
C           -------------------
C
            RKNORM = PROSCR(P2,P2,NTDL)
C
C           TEST D'ARRET DES ITERATIONS
C           ---------------------------
C
            IF(RKNORM.LE.EPS1) GO TO 10
c
c si la norme a diminue, on garde le resultat
        if (rknorm.lt.epslas) then
        do 1789 i=1,ntdl
        xcop(i) = x(i)
1789    continue
        epslas = rknorm
cwrite (imp,*) ' resultat intermediaire stocke dans xcop'
        endif
            IF(IMPR1.GE.4) WRITE (IMPRIM,1005) K,RKNORM,EPS1
c
c message a afficher pour suivre les iterations
             if (idump_solgc .ge. 2) write (imprim,1005) k,rknorm,eps1
C
C           TEST D'ARRET DU PROGRAMME
C           -------------------------
C
            IF(K.GE.ITEMAX) GO TO 12
            IF(IMPR1.GE.7) WRITE (IMPRIM,1004) (I,X(I),I=1,10)
C
C           BETAK =  ( R0 , RK+1 ) / ( RO , RK )
C           ------------------------------------
C
            BETAK = 1.D0 / R0R
            R0R = PROSCR(R1,P2,NTDL)
            BETAK = BETAK * R0R
C
C           (U) = (R) + BETAK * (H)
C           (G) = (U) + BETAK * ( BETAK * G + H )
C           -------------------------------------
C
            DO 23 I=1,NTDL
            P1(I)=P2(I)+BETAK*AP(I)
   23       R2(I)=P1(I)+BETAK*(BETAK*R2(I)+AP(I))
            GO TO 8
C
C     FIN DE LA BOUCLE SUR LES ITERATIONS DE L'ALGORITHME
C     ---------------------------------------------------
C
   10   continue
         CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,X,AP)
         DO 14 I=1,NTDL
c        R1(I)=B(J,I)-AP(I)
         R1(I)=B(I,J)-AP(I)
c        B(J,I) = X(I)
         B(I,J) = X(I)
   14    CONTINUE
         RKNORM=PROSCR(R1,R1,NTDL)
         RKNORM=sqrt (RKNORM)
      IF(IMPR1.GE.2) WRITE (IMPRIM,1000) J,K,RKNORM
      write (imprim,1006) k,rknorm
      IF(IMPR1.GE.8) WRITE (IMPRIM,1003) J,(I,X(I),I=1,NTDL)
   11 CONTINUE
      RETURN
C
   12 WRITE (IMPRIM,1001) ITEMAX
      if (istgrc.eq.0) then
        write (imprim,*) ' on repart du meilleur resultat obtenu',epslas
        write (imprim,*) '    objectif                          ',eps1
        call coptr (xcop,x,ntdl)
      else
        call coptr (xcop,x,ntdl)
        k = 999
ccall sortie ('ARRET DANS SOLGC')
      endif
      END
      SUBROUTINE  DGRA1Rdp(NTDL,NDSM,EPS,INITB0,
     &                   MAT4,MAT5,A,B0,B,MAC4,MAC5,CA,MAC7,
     &                   R1,R2,P1,P2,AP,ATP,Z,X,xcop,istgrc,k)
        use fichier_mod
        include 'dprec.h'
        include 'dump.h'
        include 'connecte_int.h'
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   BUT : ITERATIONS DE DOUBLE GRADIENT CONJUGUE ACCELEREES
C   ----- AVEC PRECONDITIONNEMENT
C
C          VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   --------------------
C   NTDL   : ORDRE DE LA MATRICE A
C   NDSM   : NBRE DE SECONDS MEMBRES
C   EPS    : PARAMETRE D ARRET DES ITERATIONS DU GRADIENT CONJUGUE
C   INITB0 : 0 INITIALISATION  PAR ZERO
C            1 INITIALISATION  PAR LE TABLEAU B0
C   MAT4   : POINTEUR AMAT4 SUR LES COEFFICIENTS DIAGONAUX
C   MAT5   : POINTEUR AMAT5 SUR LES COLONNES
C   A      : MATRICE DU SYSTEME LINEAIRE
C   B0     : TABLEAU DES VALEURS INITIALES DU G.C.
C   B      : SECOND MEMBRE
C   MAC4   : POINTEUR AMAC4 SUR LES COEFFICIENTS DIAGONAUX
C   MAC5   : POINTEUR AMAC5 SUR LES COLONNES
C   CA     : MATRICE DE PRECONDITIONNEMENT
C   MAC7   : POINTEUR SUR L ET U DANS CA
C   R1     : VECTEUR AUXILIAIRE
C   R2     : VECTEUR AUXILIAIRE
C   P1     : VECTEUR AUXILIAIRE
C   P2     : VECTEUR AUXILIAIRE
C   AP     : VECTEUR AUXILIAIRE
C   ATP    : VECTEUR AUXILIAIRE
C   Z      : VECTEUR AUXILIAIRE
C   X      : VECTEUR AUXILIAIRE
C
C   PARAMETRES DE SORTIE:
C   ---------------------
C   B      : LE TABLEAU B EST ECRASE PAR LES SOLUTIONS
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c        save ipass
c     DIMENSION A(*),B(NDSM,*),B0(NDSM,*),R1(*),R2(*),P1(*),P2(*)
      DIMENSION A(*),B(NTDL,*),B0(NTDL,*),R1(*),R2(*),P1(*),P2(*)
     &,AP(*),ATP(*),Z(*),X(*),CA(*)
      DIMENSION MAT4(*),MAT5(*),MAC4(*),MAC5(*),MAC7(*),xcop(*)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON /TRAVA1/NX(29),IMPRE,IFILL(3)

	real*8 ca

1000  FORMAT(' nb d iterations du',i5,'-eme systeme=',i7,
     S ' norme du residu',g13.6)
1001  FORMAT(' ERREUR DGRA1R : NON CONVERGENCE APRES',I8,' ITERATIONS')
1002  FORMAT(' SYSTEME NO',I8/1X,130('-')/
     &,' RKNORM=',G13.6,' ITEMAX=',I5)
1003  FORMAT(' SOLUTION DU ',I5,'-EME SYSTEME'/1X,130('-')/
     &5(' X(',I5,')=',G14.7))
1004  FORMAT(5(' X(',I5,')=',G14.7))
1005  FORMAT(' iteration',i5,' norme l2 residu=',
     &g13.6,' eps1 =',g13.6,' resmax =',g13.6,' en',i8)
1006  FORMAT('       nb d iterations de G.Conj. ',i5,
     S ' norme du residu',g13.6)
c        data ipass/1/
C
C     INITIALISATIONS DES CONSTANTES
C     ------------------------------
C
c     write (imp,'(10i5)') (mat4(i),i=1,ntdl+1)
c     write (imp,'(10i5)') (mat5(i),i=1,mat4(ntdl+1))
c     write (imp,'(8(e11.4,1x))') (a(i),i=1,mat4(ntdl+1))
c     write (imp,'(8(e11.4,1x))') (b(i,1),i=1,ntdl)
ccall sortie ('STOP DGRA1R')

cwrite (imp,*) ' dgra1r...'

      IMPR1  = IABS(IMPRE)
        un = 1.
cif (ipass.eq.1) then
c     ITEMAX = NTDL / 5
c     itemax = ntdl
cipass = 2
celse
citemax = 20
cendif
        itemax = k
      NCODSA = -1
C
C    BOUCLE SUR LES NDSM SYSTEMES A RESOUDRE
C    ---------------------------------------
C
       DO 11 J=1,NDSM
C
C    INITIALISATIONS
C    ---------------
C
   13    DO 9 I=1,NTDL
c        Z(I)= B(J,I)
         Z(I)= B(I,J)
         IF(INITB0.NE.0) GOTO 7
         X(I) = 0.
         GOTO 9
c   7    X(I) = B0(J,I)
    7    X(I) = B0(I,J)
    9    CONTINUE
C
C     (R) = (B) - ((A)) * (B0)
C     (G) = (B) - ((A)) * (B0)
C     (U) = (B) - ((A)) * (B0)
C     ------------------------
C
         call maxtab (x,ntdl,rmax)
         eps1 = 0.
         if (rmax.ne.0.) then
c
c calcul de l'objectif par une perturbation de la solution:
c on injecte dans l'equation une solution petite devant la solution
c initiale
            call initr (xcop, ntdl, rmax * eps)
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,xcop,AP)
            CALL DGRA2Rdp(NTDL,MAC4,MAC5,CA,MAC7,AP,R1)
            RKNORM=PROSCR(R1,R1,NTDL)
            eps1 = rknorm
	 else
c
c si le second membre est nul egalement, on a termine
	    call maxtab (b(1,j),ntdl,smmax)
	    if (smmax .eq. 0.) then
	       write (imprim,*) ' Systeme trivialement nul'
               k = 1
	       goto 11
	    endif
         endif
         CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,X,AP)
         CALL CL2VER(NTDL, un,Z, -un,AP,ATP)
         CALL DGRA2Rdp(NTDL,MAC4,MAC5,CA,MAC7,ATP,R1)
         DO 30 I=1,NTDL
         R2(I)=R1(I)
         P1(I)=R1(I)
         P2(I)=R1(I)
   30    CONTINUE
C
         RKNORM=PROSCR(R1,R1,NTDL)
         R0R=RKNORM
C
         K = 0
         if (eps1.eq.0.) EPS1 = EPS * EPS * RKNORM

        epslas = 1.d30
         IF(IMPR1.GE.3) WRITE (IMPRIM,1002) J,RKNORM,ITEMAX
c        write (imprim,1002) j,rknorm,itemax
         IF(RKNORM .NE. 0.) GOTO 8
         IF(INITB0 .EQ. 0 ) GOTO 10
         INITB0 = 0
         GOTO 13
C
C      ITERATIONS DE L'ALGORITHME
C      --------------------------
C
    8       K = K + 1
C
C           ALPHAK  = ( R0 , R ) / ( R0 , (A) * G )
C           ---------------------------------------
C
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,R2,ATP)
            CALL DGRA2Rdp(NTDL,MAC4,MAC5,CA,MAC7,ATP,AP)
            R0AG = PROSCR(R1,AP,NTDL)
            ALPHAK = R0R / R0AG
C
C           (H) =  (U) -  ALPHAK * ((A)) * (G)
C           (X) =  (X) +  ALPHAK * ( U + H )
C           ----------------------------------
C
            DO 21 I=1,NTDL
            AP(I)=P1(I)-ALPHAK*AP(I)
            ATP(I)=P1(I)+AP(I)
   21       X(I)=X(I)+ALPHAK*ATP(I)
C
C           (R) = (R) - ALPHAK * ((A)) * ( U + H )
C           --------------------------------------
C
            CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,ATP,Z)
            CALL DGRA2Rdp(NTDL,MAC4,MAC5,CA,MAC7,Z,ATP)
            DO 22 I=1,NTDL
   22       P2(I)=P2(I)-ALPHAK*ATP(I)
C
C           RKNORME = ( R , R )
C           -------------------
C
            RKNORM = PROSCR(P2,P2,NTDL)
C
C           TEST D'ARRET DES ITERATIONS
C           ---------------------------
C
            IF(RKNORM.LE.EPS1) GO TO 10
c
c si la norme a diminue, on garde le resultat
        if (rknorm.lt.epslas) then
        do 1789 i=1,ntdl
        xcop(i) = x(i)
1789    continue
!       iul = 1
!       call connecte ('solution',iul,iopen,'cfor')
!       write (iul,*) (xcop(i),i=1,ntdl)
!       close (iul)
!       call free_ul(iul)
        epslas = rknorm
	if (idump_solgc .ge. 2) then
           write (imprim,*) ' resultat intermediaire stocke dans xcop',
     1                ' epslas =',epslas
         endif
        endif
        IF(IMPR1.GE.4) WRITE (IMPRIM,1005) K,RKNORM,EPS1
c
c message a afficher pour suivre les iterations
        if (idump_solgc .ge. 2) then
           call maxtab2 (p2,ntdl,resmax,imax)
           write (imprim,1005) k,rknorm,eps1,resmax,imax
        endif
C
C           TEST D'ARRET DU PROGRAMME
C           -------------------------
C
            IF(K.GE.ITEMAX) GO TO 12
            IF(IMPR1.GE.7) WRITE (IMPRIM,1004) (I,X(I),I=1,10)
C
C           BETAK =  ( R0 , RK+1 ) / ( RO , RK )
C           ------------------------------------
C
            BETAK = 1.D0 / R0R
            R0R = PROSCR(R1,P2,NTDL)
            BETAK = BETAK * R0R
C
C           (U) = (R) + BETAK * (H)
C           (G) = (U) + BETAK * ( BETAK * G + H )
C           -------------------------------------
C
            DO 23 I=1,NTDL
            P1(I)=P2(I)+BETAK*AP(I)
   23       R2(I)=P1(I)+BETAK*(BETAK*R2(I)+AP(I))
            GO TO 8
C
C     FIN DE LA BOUCLE SUR LES ITERATIONS DE L'ALGORITHME
C     ---------------------------------------------------
C
   10   continue
         CALL AMATBR(NCODSA,NTDL,MAT4,MAT5,A,X,AP)
         DO 14 I=1,NTDL
c        R1(I)=B(J,I)-AP(I)
         R1(I)=B(I,J)-AP(I)
c        B(J,I) = X(I)
         B(I,J) = X(I)
   14    CONTINUE
         RKNORM=PROSCR(R1,R1,NTDL)
         RKNORM=sqrt (RKNORM)
      IF(IMPR1.GE.2) WRITE (IMPRIM,1000) J,K,RKNORM
      if (idump_solgc .ge. 1) write (imprim,1006) k,rknorm
      IF(IMPR1.GE.8) WRITE (IMPRIM,1003) J,(I,X(I),I=1,NTDL)
   11 CONTINUE
      RETURN
C
   12 WRITE (IMPRIM,1001) ITEMAX
      if (istgrc.eq.0) then
        write (imprim,*) ' on repart du meilleur resultat obtenu',epslas
        write (imprim,*) '    objectif                          ',eps1
        call coptr (xcop,x,ntdl)
      else
        call coptr (xcop,x,ntdl)
        k = 999
ccall sortie ('ARRET DANS SOLGC')
      endif
      END
      SUBROUTINE DGRA2R(NTDL,MAC4,MAC5,CA,MAC7,R,Z)
        include 'dprec.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP DESCEND LE SYSTEME TRIANGULAIRE INFERIEUR:
C -----                     (( L CA )) (Z) = (R)
C       REMONTE LE SYSTEME TRIANGULAIRE SUPERIEUR:
C                           (( U CA )) (Z) = (Z)
C
C       VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   CA             : LA MATRICE DE CONDITIONNEMENT
C   MAC4,MAC7,MAC5 : LES POINTEURS ASSOCIES
C   R              : SECOND MEMBRE
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   Z              : VECTEUR SOLUTION
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION CA(*),Z(NTDL),R(NTDL)
      DIMENSION MAC4(*),MAC5(*),MAC7(NTDL)
      DOUBLE PRECISION S
	real*4 ca
C
C     LA DESCENTE  ( L CA )  (Z) = (R)
C     --------------------------------
C
      DO 1 I=1,NTDL
      S=R(I)
      DO 2 K=MAC4(I)+1,MAC7(I)
      J=MAC5(K)
      S=S-CA(K)*Z(J)
2     CONTINUE
      Z(I)=S
1     CONTINUE
C
C     LA REMONTEE   ( U CA )  (Z) = (Z)
C     ---------------------------------
C
      DO 3 I=NTDL,1,-1
      S=Z(I)
      DO 4 K=MAC7(I)+1,MAC4(I+1)-1
      J=MAC5(K)
      S=S-CA(K)*Z(J)
4     CONTINUE
      Z(I)=S*CA(MAC4(I+1))
3     CONTINUE
      RETURN
C
      END
      SUBROUTINE DGRA2Rdp(NTDL,MAC4,MAC5,CA,MAC7,R,Z)
        include 'dprec.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP DESCEND LE SYSTEME TRIANGULAIRE INFERIEUR:
C -----                     (( L CA )) (Z) = (R)
C       REMONTE LE SYSTEME TRIANGULAIRE SUPERIEUR:
C                           (( U CA )) (Z) = (Z)
C
C       VERSION SIMPLE PRECISION
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   CA             : LA MATRICE DE CONDITIONNEMENT
C   MAC4,MAC7,MAC5 : LES POINTEURS ASSOCIES
C   R              : SECOND MEMBRE
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   Z              : VECTEUR SOLUTION
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION CA(*),Z(NTDL),R(NTDL)
      DIMENSION MAC4(*),MAC5(*),MAC7(NTDL)
      DOUBLE PRECISION S
	real*8 ca
C
C     LA DESCENTE  ( L CA )  (Z) = (R)
C     --------------------------------
C
      DO 1 I=1,NTDL
      S=R(I)
      DO 2 K=MAC4(I)+1,MAC7(I)
      J=MAC5(K)
      S=S-CA(K)*Z(J)
2     CONTINUE
      Z(I)=S
1     CONTINUE
C
C     LA REMONTEE   ( U CA )  (Z) = (Z)
C     ---------------------------------
C
      DO 3 I=NTDL,1,-1
      S=Z(I)
      DO 4 K=MAC7(I)+1,MAC4(I+1)-1
      J=MAC5(K)
      S=S-CA(K)*Z(J)
4     CONTINUE
      Z(I)=S*CA(MAC4(I+1))
3     CONTINUE
      RETURN
C
      END
      FUNCTION SPDOT(N,SY,INDEX,SX)
        include 'dprec.h'
      INTEGER N,INDEX(1:N),I
c     REAL SY(*),SX(N)
      dimension SY(*),SX(N)
      DOUBLE PRECISION X
      X = 0
      DO 10 I=1,N
        X = X + SY(INDEX(I))*SX(I)
10    CONTINUE
      SPDOT = X
      END
      SUBROUTINE SPAXPY(N,SA,SX,SY,INDEX)
        include 'dprec.h'
      INTEGER N,INDEX(1:N),I
c     REAL SA,SX(1:N),SY(*)
      dimension SX(1:N),SY(*)
      DO 10 I=1,N
        SY(INDEX(I))=SA*SX(I)+SY(INDEX(I))
10    CONTINUE
      END
      FUNCTION PROSCR(X,Y,NTDL)
        include 'dprec.h'
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                      FONCTION PROSCR
C                      ---------------
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : PRODUIT SCALAIRE DE 2 VECTEURS X Y REELS SIMPLE PRECISION
C ----- DE NTDL COMPOSANTES
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C PROGRAMMEUR : A.PERRONNET LABO ANALYSE NUMERIQUE ET IRIA PARIS  12/79
C ......................................................................
      DOUBLE PRECISION S
      DIMENSION X(*),Y(*)
C
      S=0.D0
            DO 1 I=1,NTDL
            S=S+X(I)*Y(I)
    1       CONTINUE
      PROSCR=S
C
      END
