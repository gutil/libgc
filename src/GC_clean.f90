!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: GC_clean.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

subroutine purge_noms_fichiers ()

  use tempo_mod

	implicit none
	integer(4) i,l,iul,iulfree
        character(100) form
	include 'entsor.h'
        logical b_open

!
! adaptation du format
        l = 0
	do i=1,n_noms_fichiers
	   if (len_trim (noms_fichiers(i)) .gt. l) l = len_trim (noms_fichiers(i))
	enddo
        form = '(a,a20,a,i4)'
        write (unit=form(5:6),fmt='(i2)') l+1
!
! recherche d'une unite logique libre
        iulfree = 0
        call trunit (iulfree)
	do i=1,n_noms_fichiers
! 27/03/15: on n'est pas sur de l'UL. On commence par un inquire
           inquire (file=trim (noms_fichiers(i)),opened=b_open,number=iul)
           if (.not. b_open) then
! on l'attache pour le detruire
              iul = iulfree ! si non attache, l'unite renvoyee est -1
              open (file=trim (noms_fichiers(i)),unit=iul)
           endif
           write (6,form) ' destruction de ',trim (noms_fichiers(i)) !,' UL',iul
           close (unit=iul,status='delete')
	enddo
        n_noms_fichiers = 0

   end subroutine purge_noms_fichiers
