/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: teste_GC.c
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include "GC.h"

int main(int argc, char **argv)
{

  int    mat4 [5]  = {0,  2,      5,      8,   10};
  int    mat5 [10] = {  2,1,  1,3,2,  2,4,3,  3,4};
  double mat6 [10] = {  2,1,  1,2,1,  1,2,1,  1,2};
  double b[4] = {3,4,4,3};
  double sol[4];
  int sw_int[12];
  double sw_reel[10];

  int ndl = 4;			// nombre d'inconnues
  int lmat5 = mat4[ndl+1];	// longueur des tableaux mat5 et mat6
  int nsys = 1;			// nombre de systemes a resoudre
  int initb0 = 0;		// 0 si on ne fournit pas de solution initiale
  int nivprop = 0;		// niveau de preconditionnement suggere
  int lmac5prop = 0;		// longueur suggeree de la matrice de preconditionnnement
  int niter = 100;		// nombre maximum d'iterations par niveau
  int itrait = 1;		// itrait = 1 pour faire la resolution complete
  int istgrc = 1;		// 1 pour s'arreter en cas de non convergence
  int i_cal_pc = 1;             // 1 pour calculer la matrice de preconditionnement
				// 0 si elle est deja connue
  int ieco = 0;			// 1 pour utiliser une matrice de preconditionnement
				// en simple precision
  double epsgc = 1.e-9;		// critere de convergence
  int deg_max_gc = 10;		// degre de preconditionnement maximum
  int message;			// message d'erreur
  int appli = 1;                // numero d'application
  int i;

sw_int[0] = nsys;
sw_int[1] = initb0;
sw_int[2] = nivprop;
sw_int[3] = lmac5prop;
sw_int[4] = niter;
sw_int[5] = itrait;
sw_int[6] = istgrc;
sw_int[7] = i_cal_pc;
sw_int[8] = ieco;
sw_int[9] = deg_max_gc;
sw_int[10] = message;
sw_int[11] = appli;
sw_reel[0] = epsgc;

  //gc_set_dump_ ("solgc",2);

  gc_init_sys_(mat4,mat5,&ndl);

  gc_solve_ (mat6,&lmat5,b,&ndl,sol,sol,sw_int,sw_reel);
  purge_noms_fichiers_();
  printf ("solution \n");
  for (i=0;i<4;i++)
    printf ("%f\n",sol[i]);
  GC_clean_gc();
}

