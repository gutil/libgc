      SUBROUTINE CAPLU1(NTDL,MAC4,MAC5,MAC7,lmac5)

        include 'dprec.h'
        include 'dump.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                    S.P. CAPLU1
C                    -----------
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP CALCULE LES POINTEURS MAC4,MAC5
C ----  CORRESPONDANTS AU 'REMPLISSAGE' DE LA
C       MATRICE EN COURS DE FACTORISATION
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   MAC4,MAC7,MAC5 : LES POINTEURS ASSOCIES
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   MAD4 ,MAD5     : LES POINTEURS CHERCHES
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION MAC4(*),MAC7(NTDL)

	integer(4),allocatable :: mad5(:),mad4(:),ind(:)
	integer(4),pointer :: mac5(:)

	include 'allonge_tab_int_int.h'
	include 'entsor.h'

      if (allocated (mad4)) deallocate (mad4)
      allocate (mad4(ntdl+1),stat=ires)
      call diag_alloc (ires,'mad4 (solgc)')
      if (allocated (ind)) deallocate (ind)
      allocate (ind(ntdl),stat=ires)
      call diag_alloc (ires,'ind (solgc)')
C
C     CALCUL DU REMPLISSAGE : TABLEAUX MAD4 ET MAD5
C

cwrite (imp,*) ' caplu1...'

!
! boucle sur la taille de mad5, inconnue a priori
!
      if (lmac5 .eq. 0) lmac5 = mac4(ntdl+1)
      lmad5 = 2*lmac5

      try:do

      if (allocated (mad5)) deallocate (mad5)
      allocate (mad5(lmad5),stat=ires)
      call diag_alloc (ires,'mad5 (solgc)')

      DO 1 I=1,NTDL
      IND(I)=0
1     CONTINUE
      IC1=0
      IC2=0
      K1=1
      DO 2 I=1,NTDL
      K2=MAC4(I+1)
      DO 3 K=K1,K2
      J=MAC5(K)
      IND(J)=I
3     CONTINUE
      DO 5 K=K1,MAC7(I)
      J=MAC5(K)
      DO 5 L=MAC7(J)+1,MAC4(J+1)
      JJ=MAC5(L)
      IF(IND(JJ).NE.I) THEN
      IC1=IC1+1
cWrite (imp,*) 'ic1',ic1
! modif le 15/2/2005: on reserve 3 fois au lieu de 2
! modif annulee
!     if (ic1 .gt. 2*lmac5) then
      if (ic1 .gt. lmad5) then
! write (imp,*) 'ic1, 2*lmac5 ',ic1,2*lmac5
! call sortie ('CALPLU1 1')
!
! 3/08/06: on agrandit le tableau mad5 et on recommence

         lmad5 = lmad5+lmac5
	 write (imp,*)
     1   ' Agrandissement du tableau mad5, nouvelle taille =',lmad5
         cycle try
      endif
      MAD5(IC1)=JJ
      IND(JJ)=I
      END IF
5     CONTINUE
4     CONTINUE
      MAD4(I+1)=IC1
      IC2=IC1
      K1=K2+1
2     CONTINUE
      MAD4(1)=0
C
C     REUNION DE MAC5 ET MAD5 DANS MAC5
C
      IF(MAD4(NTDL+1).GT.0) THEN
      KC2=MAC4(NTDL+1)+MAD4(NTDL+1)
!     if (kc2 .gt. lmac5) then
      if (kc2 .gt. size(mac5)) then
!call sortie  ('LMAC5 TROP PETIT')
! deallocate (mac5)
! allocate (mac5(kc2),stat=ires)
! call diag_alloc(ires,' mac5 (calplu1)')
	 if (idump_solgc .gt. 1)
     1   write (imp,*) ' Extension de mac5 a',kc2,' mots'
	 call allonge_tab_int (mac5,kc2,ires)
	 lmac5 = kc2
      endif

      DO 6 I=NTDL,1,-1
      KC2A=MAC4(I+1)
      KD2=MAD4(I+1)+1
      LC=MAC4(I+1)-MAC4(I)-1
      LD=MAD4(I+1)-MAD4(I)
      MAC4(I+1)=KC2
cWrite (imp,*) 'kc2',kc2
      MAC5(KC2)=I
      DO 7 J=1,LD
cWrite (imp,*) 'kc2-j',kc2-j
      MAC5(KC2-J)=MAD5(KD2-J)
7     CONTINUE
      KC2=KC2-LD
      DO 8 J=1,LC
cWrite (imp,*) 'kc2-j',kc2-j
      MAC5(KC2-J)=MAC5(KC2A-J)
8     CONTINUE
      KC2=KC2-LC
C     ON ORDONNE LES D.L. DE LA LIGNE I PAR ORDRE CROISSANT
      KC=MAC4(I+1)-1
      DO 9 K=KC2,KC
      DO 9 L=K,KC
      IF(MAC5(K)-MAC5(L)) 9,9,10
10    MACK=MAC5(K)
cWrite (imp,*) 'k',k
      MAC5(K)=MAC5(L)
cWrite (imp,*) 'l',l
      MAC5(L)=MACK
9     CONTINUE
      KC2=KC2-1
6     CONTINUE
      MAC4(1)=0
      END IF

      exit try
!
! fin de la boucle d'essai
      enddo try

      if (allocated(mad4)) deallocate (mad4)
      if (allocated(mad5)) deallocate (mad5)
      if (allocated(ind)) deallocate (ind)
C
      RETURN
      END
