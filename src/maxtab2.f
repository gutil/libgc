      subroutine maxtab2 (tab,n,rm,nurm)
        include 'dprec.h'

        include 'entsor.h'

c        data ipass/1/
c        save ipass
c ----------------------------------------------------------------------
c maximum en valeur absolue d'un tableau
c
      dimension tab(*)
      common /contro/ impres
      if (impres.ge.1) write (imp,*) ' >> maxtab'
      rm=0.
      nurm = 1
      do 100 i=1,n
         rr=tab(i)*tab(i)
         if (rr.gt.rm) then
                rm=rr
                nurm = i
           endif
100   continue
      rm = sqrt (rm)
      end
