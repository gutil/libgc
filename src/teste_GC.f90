!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: teste_GC.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

use tempo_mod

real(8),allocatable :: mat6(:),b(:),sol(:)
integer(4),allocatable :: mat5(:),mat4(:)

integer(4) :: deg_max_gc
integer(4) :: sw_int(12)
real(8) :: sw_reel(10)

include 'dump.h'
common /entsor/ lec,imp

lec = 5
imp = 6

call GC_set_dump ('solgc',0)

!
! resolution d'un petit systeme (4 eqt, 4 inconnues)

ndl = 4

allocate (mat4(ndl+1))
mat4 = (/ 0,2,5,8,10 /)
lmat5 = mat4(ndl+1); write (6,*) ' lmat5 =',lmat5
allocate (mat5(lmat5))
mat5 = (/ 2,1, 1,3,2, 2,4,3,  3,4/)
allocate (mat6(lmat5))
mat6 = (/2.,1.,  1.,2.,1.,  1.,2.,1.,  1.,2./)
allocate (b(ndl))
b = (/3.,4.,4.,3./)
allocate (sol(ndl))

call GC_init_sys (mat4,mat5,ndl)

nsys = 1
initb0 = 0
nivprop = 0
lmac5prop = 0
niter = 10
itrait = 1
istgrc = 1
i_cal_pc = 1
ieco = 0
epsgc = 1.e-9
deg_max_gc = 10
iappli = 1

sw_int(1) = nsys
sw_int(2) = initb0
sw_int(3) = nivprop
sw_int(4) = lmac5prop
sw_int(5) = niter
sw_int(6) = itrait
sw_int(7) = istgrc
sw_int(8) = i_cal_pc
sw_int(9) = ieco
sw_int(10) = deg_max_gc
sw_int(11) = message
sw_int(12) = iappli
sw_reel(1) = epsgc
sw_reel(2:10) = 0.

call GC_solve (mat6,lmat5,b,ndl,sol,sol,sw_int,sw_reel)

write (6,'(a/(f10.6))') ' solution',sol

!
! gros systeme

deallocate (mat4)
deallocate (mat5)
deallocate (mat6)
deallocate (b)
deallocate (sol)

open (unit=1,file='systeme',form='unformatted')
read (1) ndl;write (6,*) ' ndl =',ndl
read (1) lbid;write (6,*) ' lbid =',lbid

allocate (mat4(ndl+1))
read (1) mat4
lmat5 = mat4(ndl+1); write (6,*) ' lmat5 =',lmat5
allocate (mat5(lmat5))
read (1) mat5
allocate (mat6(lmat5))
read (1) mat6
allocate (b(ndl))
read (1) b
close(1)
allocate (sol(ndl))

iappli = 10
sw_int(12) = iappli

call GC_init_sys (mat4,mat5,ndl)

call GC_solve (mat6,lmat5,b,ndl,sol,sol,sw_int,sw_reel)

write (6,'(a/(10f8.2))') ' solution',sol

!
! petit systeme, bis

deallocate (mat4)
deallocate (mat5)
deallocate (mat6)
deallocate (b)
deallocate (sol)

ndl = 4

allocate (mat4(ndl+1))
mat4 = (/ 0,2,5,8,10 /)
lmat5 = mat4(ndl+1)
allocate (mat5(lmat5))
mat5 = (/ 2,1, 1,3,2, 2,4,3,  3,4/)
allocate (mat6(lmat5))
mat6 = (/2.,1.,  1.,2.,1.,  1.,2.,1.,  1.,2./)
allocate (b(ndl))
b = (/3.,4.,4.,3./)
allocate (sol(ndl))

iappli = 100
sw_int(12) = iappli

call GC_init_sys (mat4,mat5,ndl)

call GC_solve (mat6,lmat5,b,ndl,sol,sol,sw_int,sw_reel)

write (6,'(a/(f10.6))') ' solution',sol

call purge_noms_fichiers

end

