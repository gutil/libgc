#pragma once

#include "spDefs.h"

//NF 11/7/2011 from spAllocate
void InitializeElementBlocks(MatrixPtr, int ,int );
void RecordAllocation(MatrixPtr,char  *);
void AllocateBlockOfAllocationList(MatrixPtr);
void ExpandTranslationArrays(MatrixPtr ,register int);

//NF 11/7/2011 from spBuild
void Translate(MatrixPtr,int  *,int  *);
void EnlargeMatrix(MatrixPtr,register int);

//NF 11/7/2011 from spFactor
ElementPtr CreateFillin(MatrixPtr,register int,int);

//NF 11/7/2011 from spSolve
void SolveComplexMatrix(MatrixPtr,RealVector,RealVector);
void SolveComplexTransposedMatrix(MatrixPtr,RealVector,RealVector);

//NF 11/7/2011 from spUtils
int CountTwins(MatrixPtr,int,ElementPtr *,ElementPtr *);
void SwapCols(MatrixPtr,ElementPtr,ElementPtr);
void ScaleComplexMatrix(MatrixPtr,register  RealVector,register  RealVector);
void ComplexMatrixMultiply(MatrixPtr,RealVector,RealVector);
void ComplexTransposedMatrixMultiply(MatrixPtr,RealVector,RealVector);
RealNumber ComplexCondition(MatrixPtr,RealNumber,int *);

//NF 11/7/2011 from spTest
int ReadMatrixFromFile();
int Init(RealNumber *,char *,int,int);
ComplexVector CheckOutComplexArray(int);
void CheckInAllComplexArrays();
void PrintMatrixErrorMessage(int);
int InterpretCommandLine(int,char **);
char *GetProgramName(char *);
void Usage(char *);
void EnlargeVectors(int,int);



ElementPtr QuicklySearchDiagonal(MatrixPtr ,int  );
ElementPtr SearchDiagonal(MatrixPtr ,register int  );
void spcLinkRows(MatrixPtr Matrix);
void spcRowExchange(MatrixPtr Matrix, int Row1, int Row2);
void spcColExchange(MatrixPtr Matrix,int Col1,int Col2);
