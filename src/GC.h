/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: GC.h
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


#include "param_GC.h"
#include "struct_GC.h"
#include "function_GC.h"
