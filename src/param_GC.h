/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: param_GC.h
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


#define NSYS_GC 1 // nombre de systemes a resoudre NS_GC dans enum config_gc_int
#define INITB0_GC 0 // 0 si on ne fournit pas de solution initiale B0_GC dans enum config_gc_int
#define NIVPROP_GC 0 // niveau de preconditionnement suggere
#define LMAC5PROP_GC 0 // longueur suggeree de la matrice de preconditionnnement
#define NITER_GC 100 // nombre maximum d'iterations par niveau de preconditionnement
#define ITRAIT_GC 1 // itrait = 1 pour faire la resolution complete, itrait = 2 pour ne pas recalculer les pointeurs de préconditionnement
#define ISTGRC_GC 1 // 1 pour s'arreter en cas de non convergence
#define ICALCPC_GC 1 // 1 pour calculer la matrice de preconditionnement // 0 si elle est deja connue
#define IECO_GC 1 // 1 pour utiliser une matrice de preconditionnement en simple precision
#define DEGMAX_GC 10// degre de preconditionnement maximum
#define EPS_GC 0.000000001 // critere de convergence

enum config_gc_int {NS_GC,B0_GC,NIVPRECOND_GC,LPRECOND_GC,NITMAX_GC,TRAIT_GC,STGRC_GC,CALCPC_GC,ECO_GC,PRECONDMAX_GC,MSG_GC,APPL_NB,NINT_STRUCT_GC} ;
enum appl_nb {AQ_GC=1,HYD_GC=2,TTC_GC=3,HDERM_GC=4, TTC_AQ_GC=5}; // NG : 15/02/2020 : Muskingum-HDERM resolution application added

#define IAPPLI 12 //Number of systems that can be solved by libgc

#define VERSION_GC 0.13
