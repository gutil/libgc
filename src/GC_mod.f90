!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: GC_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module GC_mod

  ! tableaux de travail a dimension variable pour le gradient conjugue

  real(8),allocatable,save,target :: secm(:)
  ! integer(4),allocatable,save :: mat4(:)
  ! integer(4),allocatable,save :: mat5(:)
  integer(4),pointer,save :: mat4(:)=>null()
  integer(4),pointer,save :: mat5(:)=>null()
  real(8),pointer,save :: mat6(:)=>null()
  real(8),allocatable,save :: mat6t(:)
  real(8),allocatable,save :: mat6a(:)
  !integer(4),allocatable,save :: mac4(:)
  !integer(4),pointer,save :: mac5(:)=>null()
  !integer(4),allocatable,save :: mac7(:)

  integer(4),parameter :: max_niv=10
  integer(4),parameter :: max_nivp1=max_niv+1
  integer(4),parameter :: max_nivm3=max_nivp1*3
  integer(4),parameter :: max_appli=100
  integer(4),parameter :: max_appliXmax_nivp1=max_appli*max_nivp1
  logical b_sto_mac5(0:max_niv,max_appli)
  data b_sto_mac5 /max_appliXmax_nivp1*.FALSE./
  save b_sto_mac5
  character(30) nom_mac5(0:max_niv,max_appli)
  integer iul_mac5(0:max_niv,max_appli)
  save iul_mac5
  logical b_sto_mac6(0:max_niv,3)
  data b_sto_mac6 /max_nivm3*.FALSE./
  save b_sto_mac6
  character(20) nom_mac6(0:max_niv,3)
  integer iul_mac6(0:max_niv,3)
  save iul_mac6
  logical b_sauve_mac6
  integer(4),save :: iappli_mac6

contains

  subroutine free_sys_lin

    implicit none

    !   if (allocated (mat5)) then
    if (associated (mat5)) then
       deallocate (mat5)
    endif
    if (allocated (secm)) then
       deallocate (secm)
    endif
    !   if (allocated (mat5)) then
    if (associated (mat5)) then
       deallocate (mat5)
    endif
    if (associated (mat6)) then
       deallocate (mat6)
    endif
    if (allocated (mat6t)) then
       deallocate (mat6t)
    endif
    if (allocated (mat6a)) then
       deallocate (mat6a)
    endif

  end subroutine free_sys_lin

  ! subroutine free_GC

  !   implicit none

  !   call free_sys_lin

  !   if (allocated (mac4)) then
  !      deallocate (mac4)
  !   endif
  ! if (allocated (mac5)) then
  !    deallocate (mac5)
  ! endif
  !   if (allocated (mac7)) then
  !      deallocate (mac7)
  !   endif

  ! end subroutine free_GC

  subroutine stomac5 (mac4,mac5,mac7,ntdl,niveau,iappli)

    use tempo_mod

    include 'dprec.h'
    include 'dump.h'
    include 'connecte_int.h'
    include 'entsor.h'

    !
    ! stockage sur fichier non formate de la matrice mac5
    !
    dimension mac4(*),mac5(*),mac7(*)

    idump = idump_stomac5
    nom_mac5(niveau,iappli) = 'mac5_'
    if (iappli .lt. 10) then
       write (unit=nom_mac5(niveau,iappli)(6:7),fmt='(i1,a1)') iappli,'_'  !BL comment
       if (niveau .lt.10) then !BL comment
          write (unit=nom_mac5(niveau,iappli)(8:9),fmt='(i1,a1)') niveau,'_'  !BL comment
       else  !BL comment
          write (unit=nom_mac5(niveau,iappli)(8:10),fmt='(i2,a1)') niveau,'_'  !BL comment
       endif  !BL comment 
    elseif (iappli .lt. 100) then
       write (unit=nom_mac5(niveau,iappli)(6:8),fmt='(i2,a1)') iappli,'_'  !BL comment
       if (niveau .lt.10) then !BL comment
          write (unit=nom_mac5(niveau,iappli)(9:10),fmt='(i1,a1)') niveau,'_'  !BL comment
       else  !BL comment
          write (unit=nom_mac5(niveau,iappli)(9:11),fmt='(i2,a1)') niveau,'_'  !BL comment
       endif  !BL comment 
    elseif (iappli .lt. 1000) then
       write (unit=nom_mac5(niveau,iappli)(6:9),fmt='(i3,a1)') iappli,'_'  !BL comment
       if (niveau .lt.10) then !BL comment
          write (unit=nom_mac5(niveau,iappli)(10:11),fmt='(i1,a1)') niveau,'_'  !BL comment
       else  !BL comment
          write (unit=nom_mac5(niveau,iappli)(10:12),fmt='(i2,a1)') niveau,'_'  !BL comment
       endif  !BL comment 
    else
       write (imp,'(//a//)') ' Nombre maximum d''appli depasse!!!'
       stop
    endif
    call ficunic (nom_mac5(niveau,iappli),nom_mac5(niveau,iappli))  !BL comment
    call connecte (nom_mac5(niveau,iappli),iul_mac5(niveau,iappli),iopen,"nfor")  !BL comment

    if (idump .ge. 1) write (imp,'(a,a,a,i4)') ' Stockage de mac4, mac5 et mac7 sur ',trim(nom_mac5(niveau,iappli)),' UL',iul_mac5(niveau,iappli)
    write (imp,'(a,a,a,i4)') ' Stockage de mac4, mac5 et mac7 sur ',trim(nom_mac5(niveau,iappli)),' UL',iul_mac5(niveau,iappli)

    iul = iul_mac5(niveau,iappli)
    write (iul) ntdl+1
    write (iul) (mac4(i),i=1,ntdl+1)
    write (iul) ntdl+1
    write (iul) (mac7(i),i=1,ntdl+1)
    long = mac4(ntdl+1)
    write (iul) long
    write (iul) (mac5(i),i=1,long)
    rewind (iul)
    close (iul) !to comment BL

    b_sto_mac5(niveau,iappli) = .TRUE.
    call ajoute_nom_fichier (nom_mac5(niveau,iappli),iul_mac5(niveau,iappli))

  end subroutine stomac5

  subroutine lecmac5 (mac4,mac5,mac7,niveau,iappli)

    use alloue_mod

    include 'dprec.h'
    include 'dump.h'
    include 'entsor.h'

    !
    ! relecture sur fichier non formate de la matrice mac5
    !
    integer(4),pointer :: mac4(:),mac5(:)
    integer(4) :: mac7(*)

    idump = idump_lecmac5
    iul = iul_mac5(niveau,iappli)
    if (idump .ge. 1) write (imp,'(a,a,a,i4)') ' Relecture de mac4, mac5 et mac7 sur ',trim(nom_mac5(niveau,iappli)),' UL',iul

    open (unit=iul,file=nom_mac5(niveau,iappli),form='unformatted')
    read (iul) long
    read (iul) (mac4(i),i=1,long)
    read (iul) long
    read (iul) (mac7(i),i=1,long)
    read (iul) long
    !   if (size(mac5) .lt. long) then
    call desalloue (mac5)
    call alloue (mac5,long,' mac5 (lecmac5)')
    !   endif
    read (iul) (mac5(i),i=1,long)
    rewind (iul)
    close (iul)

  end subroutine lecmac5

  subroutine stomac6 (mac6,long,niveau)

    use tempo_mod

    include 'dprec.h'
    include 'dump.h'
    include 'connecte_int.h'
    include 'entsor.h'

    character(4),save :: nappli(3)
    data nappli /'ecou','disp','ther'/
    !
    ! stockage sur fichier non formate de la matrice mac6
    !
    real(8) mac6(*)

    idump = idump_stomac6
    nom_mac6(niveau,iappli_mac6) = 'mac6_'//nappli(iappli_mac6)
    if (niveau .lt.10) then
       write (unit=nom_mac6(niveau,iappli_mac6)(10:12),fmt='(a1,i1,a1)') '_',niveau,'_'
    else
       write (unit=nom_mac6(niveau,iappli_mac6)(10:13),fmt='(a1,i2,a1)') '_',niveau,'_'
    endif
    call ficunic (nom_mac6(niveau,iappli_mac6),nom_mac6(niveau,iappli_mac6))
    call connecte (nom_mac6(niveau,iappli_mac6),iul_mac6(niveau,iappli_mac6),iopen,"nfor")

    if (idump .ge. 1) write (imp,'(a,a,a,i4)') ' Stockage de mac6 sur ',trim(nom_mac6(niveau,iappli_mac6)),' UL',iul_mac6(niveau,iappli_mac6)

    iul = iul_mac6(niveau,iappli_mac6)
    write (iul) long
    write (iul) (mac6(i),i=1,long)
    rewind (iul)
    !close (iul)

    b_sto_mac6(niveau,iappli_mac6) = .TRUE.
    call ajoute_nom_fichier (nom_mac6(niveau,iappli_mac6),iul_mac6(niveau,iappli_mac6))

  end subroutine stomac6

  subroutine lecmac6 (mac6,niveau)

    use alloue_mod

    include 'dprec.h'
    include 'dump.h'
    include 'entsor.h'

    !
    ! relecture sur fichier non formate de la matrice mac6
    !
    real(8),pointer :: mac6(:)

    idump = idump_lecmac6
    iul = iul_mac6(niveau,iappli_mac6)
    if (idump .ge. 1) write (imp,'(a,a,a,i4)') ' Relecture de mac6 sur ',trim(nom_mac6(niveau,iappli_mac6)),' UL',iul

    open (unit=iul,file=nom_mac6(niveau,iappli_mac6),form='unformatted')
    read (iul) long
    if (size(mac6) .lt. long) then
       call desalloue (mac6)
       call alloue (mac6,long,' mac6 (lecmac6)')
    endif
    read (iul) (mac6(i),i=1,long)
    rewind (iul)
    !close (iul)

  end subroutine lecmac6

  subroutine init_sauve_mac6 (iap,niv)

    use tempo_mod

    implicit none

    integer(4),optional :: iap,niv
    integer(4) idump,ia,iniv,ii,i
    include 'dump.h'
    include 'entsor.h'

    idump = idump_init_sauve_mac6

    appli:do ia=1,3
       if (present(iap)) then
          if (iap .ne. ia) cycle appli
       endif
       niveau:do iniv=0,max_niv
          if (present(niv))then
             if (niv .ne. iniv) cycle niveau
          endif
          if (b_sto_mac6(iniv,ia) .eqv. .TRUE.) then
             if (idump .ge. 1) write (imp,'(a,a,a,i4)') ' Destruction de ',trim(nom_mac6(iniv,ia)),' UL',iul_mac6(iniv,ia)
             close (unit=iul_mac6(iniv,ia),status='delete')
             !
             ! suppression dans la lliste des fichiers temporaires
             ii=0
             do i=1,n_noms_fichiers
                if (iul_fichiers(i) .eq. iul_mac6(iniv,ia)) then
                   ii = i
                   exit
                endif
             enddo
             if (ii .eq. 0) then
                write (imp,'(//a)') ' Anomalie dans init_sauve_mac6, fichier ',trim (nom_mac6(iniv,ia)), &
                     ' pas trouve dans la liste des fichiers temporaires'
                call sortie ('init_sauve_mac6')
             endif
             do i=ii,n_noms_fichiers-1
                iul_fichiers(i)=iul_fichiers(i+1)
                noms_fichiers(i)=noms_fichiers(i+1)
             enddo
             n_noms_fichiers = n_noms_fichiers-1
             b_sto_mac6(iniv,ia) = .FALSE.
          endif
       enddo niveau
    enddo appli
  end subroutine init_sauve_mac6

end module GC_mod
