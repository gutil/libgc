!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: ficunic.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

subroutine ficunic (racine,nom)

! 
! ======================================================================
! ce sous-programme cree un nom de fichier en ajoutant a la racine
! un nombre compris entre 0 et 99 et verifie qu'aucun fichier de  ce nom n'existe
!
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< arguments en entree:
!
!   racine:  chaine de caractere
!
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> arguments en sortie:
!
!   nom: le nom cree
! ======================================================================
!
implicit none

character(*), intent(in) :: racine
character(*), intent(out) :: nom
logical existe
integer(4) :: i,ideb

include 'entsor.h'

nom = trim(racine)
ideb = len_trim(nom) + 1

do i = 1,99
   write (unit=nom(ideb:ideb+1),fmt='(i2)') i
   if (i .lt. 10) nom(ideb:ideb) = '0'
   inquire (file=nom,exist=existe)
   if (.not. existe) goto 100
enddo

write (imp,'(///a/a/)') &
' Penurie de fichiers de type ',trim(racine)//'XX', &
' faire le menage S.V.P.'
call sortie ('ficunic')
100     continue

end
