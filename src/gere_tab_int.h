
	interface alloue

	  module procedure alloue_int
	  module procedure alloue_flt
	  module procedure alloue_log
	  module procedure alloue_int_2D
	  module procedure alloue_flt_2D

	end interface

	interface desalloue

	  module procedure desalloue_int
	  module procedure desalloue_flt
	  module procedure desalloue_log
	  module procedure desalloue_int_2D
	  module procedure desalloue_flt_2D

	end interface
