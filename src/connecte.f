        subroutine connecte (fichier,iul,iopen,ityf,b_check_existe)

	use fichier_mod

	character*(*) fichier,ityf
	integer(4) :: iul,iopen
	logical,optional :: b_check_existe

	include 'dump.h'

        include 'trunit_int.h'

	common /entsor/ lec,imp

c
c ce sous-programme verifie si un fichier est ouvert
c si oui, il retourne l'unite logique associee
c
c sinon, il cherche la premiere unite logique non utilisee
c a partir de iul+1, connecte le fichier et retourne iul

        logical result

	idump = idump_connecte
!
! on teste si necessaire l'existence du fichier
!
	if (present (b_check_existe)) then
	   if (b_check_existe) then
              inquire (file=fichier,exist=result)
	      if (.not. result) then
                 write (imp,*)
                 write (imp,*)
                 write (imp,'(a,a)')
     1          ' erreur a l''ouverture du fichier ',
     1           trim(fichier)
                 write (imp,*)
                 write (imp,*) '        fichier inexistant'
                 write (imp,*)
                 call sortie ('connecte')
              endif
	   endif
	endif

        inquire (file=fichier,opened=result,number = iuln)

cwrite (imp,*) ' connecte, fichier ',fichier
c
c fichier deja ouvert, on recupere son U.L..

	if (result) then
cwrite (imp,*) ' Le fichier ',fichier,' est deja ouvert'
cwrite (imp,*) ' unite logique :',iuln
	   iul = iuln
	   iopen = 1
! reaffectation de l'unite
	   call aff_ul (fichier,iul)
	   return
	endif

c
c fichier non ouvert, on lui alloue une U.L.

        iuldeb = 1
        do 100 iul=iuldeb,999
        inquire (unit=iul,opened=result)
        if (.not. result) goto 101
100     continue
        write (imp,*)
        write (imp,*)
        write (imp,*) ' erreur dans la recherche d''une unite logique'
        write (imp,*) ' disponible: pas d''unite entre',iuldeb,' et 999'
        call sortie ('CONNECTE')
101     continue
	if (idump .ge. 1) write (imp,'(a,i4,a,a)')
     1   ' connecte alloue l''UL',iul,' a ',trim(fichier)
        if (ityf .eq. 'cfor')
     1     open (unit=iul,file = fichier,form='formatted')
        if (ityf .eq. 'nfor')
     1     open (unit=iul,file = fichier,form='unformatted')
	call add_ul (fichier,iul)
	iopen = 0
        
        end
