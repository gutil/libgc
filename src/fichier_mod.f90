!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: fichier_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module fichier_mod

type ficdesc

    character*256 :: nom
    integer(4) :: ul
    integer(4) :: comptsor
    logical :: gocad

end type ficdesc

integer(4),save :: nb_fic_ouv = 0

   integer(4),parameter :: MAX_UL = 500
   integer(4),save :: nb_ul = 0
   integer(4),save :: liste_ul(MAX_UL)
   logical,save :: b_entete (MAX_UL)
   type (ficdesc),save :: l_fic_ouv(MAX_UL)

contains

   logical function a_entete (ul)

        include 'entsor.h'

        integer(4) ul

        a_entete = .FALSE.
        if (ul .eq. imp) then
           a_entete = .TRUE.
           return
        endif
        do i=1,nb_ul
           if (ul .eq. liste_ul(i)) then
              a_entete = b_entete (i)
              exit
           endif
        enddo

   end function a_entete

   subroutine set_entete (ul)

        include 'entsor.h'

        integer(4) ul

        if (ul .eq. imp) return
        nb_ul = nb_ul +1
        if (nb_ul .gt. MAX_UL) then
           write (6,*)
           write (6,*)
           write (6,*) ' Nombre maximum d''unites logiques depasse'
           call sortie ('set_entete')
        endif
        liste_ul (nb_ul) = ul
        b_entete (nb_ul) = .TRUE.

   end subroutine set_entete

subroutine aff_ul (nom,ul)

implicit none

character*(*) nom
integer(4) ul,i

boucle:do i=1,nb_fic_ouv
   if (nom .eq. l_fic_ouv(i)%nom) then
      l_fic_ouv(i)%ul = ul
      exit boucle
   endif
enddo boucle

end subroutine aff_ul

subroutine show_ul

implicit none

integer(4) i

include 'entsor.h'

boucle:do i=1,nb_fic_ouv
   write (imp,'(a,i3,a,a)') 'unite',l_fic_ouv(i)%ul,' nom ',trim(l_fic_ouv(i)%nom)
enddo boucle

end subroutine show_ul

subroutine add_ul (nom,ul)

implicit none

character*(*) nom
integer(4) ul
include 'entsor.h'

nb_fic_ouv = nb_fic_ouv + 1
!write (imp,*) ' Nombre d''unites ouvertes',nb_fic_ouv
if (nb_fic_ouv .gt. MAX_UL) then
   write (imp,'(//a//)') ' Nombre maximum d''unites logiques depasse'
   call sortie ('fichier_mod_add_ul')
endif

l_fic_ouv(nb_fic_ouv)%nom = nom
l_fic_ouv(nb_fic_ouv)%ul = ul
l_fic_ouv(nb_fic_ouv)%comptsor = 1
!write (imp,*) ' Ouverture de l''unite logique',ul,' liee a ',trim (nom)

!call show_ul

end subroutine add_ul

subroutine free_ul (ul)

implicit none

integer(4) ul,iul,i,j
include 'entsor.h'

!write (imp,*) ' Fermeture de l''unite logique',ul
iul = 0
do i=1,nb_fic_ouv
   if (l_fic_ouv(i)%ul .eq. ul) then
      iul = i
      exit
   endif
enddo
if (iul .eq. 0) then
   write (imp,'(//a//)') ' Tentative de fermer une unite non enregistree'
   call sortie ('fichier_mod_free_ul')
endif

do j = i,nb_fic_ouv -1
   l_fic_ouv(j)%nom = l_fic_ouv(j+1)%nom
   l_fic_ouv(j)%ul = l_fic_ouv(j+1)%ul
   l_fic_ouv(j)%comptsor = l_fic_ouv(j+1)%comptsor
enddo

nb_fic_ouv = nb_fic_ouv - 1
!write (imp,*) ' Nombre d''unites ouvertes',nb_fic_ouv

!call show_ul

end subroutine free_ul

subroutine set_ul_goc (ul,gocad)

implicit none

integer(4) :: ul,i
logical :: gocad

boucle:do i=1,nb_fic_ouv
   if (ul .eq. l_fic_ouv(i)%ul) then
      l_fic_ouv(i)%gocad = gocad
      exit boucle
   endif
enddo boucle

end subroutine set_ul_goc

function comptsor_from_ul (ul)

implicit none

integer(4) :: ul,i
integer(4) :: comptsor_from_ul
include 'entsor.h'

comptsor_from_ul = 0
boucle:do i=1,nb_fic_ouv
   if (ul .eq. l_fic_ouv(i)%ul) then
      comptsor_from_ul = l_fic_ouv(i)%comptsor
      exit boucle
   endif
enddo boucle
if (comptsor_from_ul .eq. 0) then
   write (imp,*) ' unite logique',ul,' pas trouvee dans la liste'
   call sortie ('comptsor_from_ul')
endif

end function comptsor_from_ul

subroutine ul_from_nom (nom,ul)

implicit none

character*(*) nom
integer(4) ul,i
include 'entsor.h'

ul = 0
boucle:do i=1,nb_fic_ouv
   if (nom .eq. l_fic_ouv(i)%nom) then
      ul = l_fic_ouv(i)%ul
      exit boucle
   endif
enddo boucle
if (ul .eq. 0) then
   write (imp,'(/a/a,a/)') &
   ' Anomalie detectee par ul_from_nom', &
   ' Pas d''unite logique connectee au fichier ',trim(nom)
   call sortie ('ul_from_nom')
endif

end subroutine ul_from_nom

subroutine inc_comptsor (ul)

implicit none

integer(4) :: ul,i

boucle:do i=1,nb_fic_ouv
   if (ul .eq. l_fic_ouv(i)%ul) then
      l_fic_ouv(i)%comptsor = l_fic_ouv(i)%comptsor+1
      exit boucle
   endif
enddo boucle

end subroutine inc_comptsor

subroutine ferme_goc

implicit none

integer(4) :: i

boucle:do i=1,nb_fic_ouv
   if (l_fic_ouv(i)%gocad) then
      write (l_fic_ouv(i)%ul,'(a)') '//'
      close (l_fic_ouv(i)%ul)
   endif
enddo boucle

end subroutine ferme_goc

end module fichier_mod
