!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: sortie.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

!
! sortie propre avec nettoyage des fichiers temporaires
!
subroutine sortie (message)

  use espace_mod
  use tempo_mod

  include 'entsor.h'

  character*(*) message

  if (message .ne. ' ') then
     write (imp,'(/a,a/)') ' Arret du programme dans la subroutine ',trim (message)
  endif

  write (imp,'(/a/)') ' Destruction des fichiers temporaires'
  !
  ! destruction de tous les fichiers temporaires

  call purge_noms_fichiers
  !
  ! fichiers de sauvegarde des tableaux
  !
  call menage_tab

  write (imp,'(//)')
  stop

end subroutine sortie
