/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: function_GC.h
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


//in manage_gc.c
void GC_init_gc(s_gc *,int);
s_gc *GC_create_gc(int);
int *GC_create_mat_int(int );
double *GC_create_mat_double(int );
void  gc_init_sys_(int*,int*,int*);//Declaration des fonctions fortran
void  gc_solve_(double*,int*,double*,int*,double*,double*,int*,double*);//Declaration des fonctions fortran
void purge_noms_fichiers_(); //Declaration des fonctions fortran
void GC_configure_gc(s_gc*, int*, FILE*); // NG : 09/06/2021
void GC_solve_gc(s_gc *,int,FILE*); // NG : 09/06/2021 : prototype modified
void GC_print_configuration_gc(s_gc*, int*, FILE*); // NG 09/06/2021
void GC_clean_gc(void);
void GC_check_consistency(s_gc*, FILE*);
void GC_fill_sparse(s_gc *,double *,void *); // SW 11/07/2018

// in itos.c
char *GC_name_iappl(int, FILE*); // NG : 31/05/2021
char *GC_name_config_param(int, FILE*); // NG : 09/06/2021
