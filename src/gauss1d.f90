!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: gauss1d.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

subroutine gauss1d (matr,br,n,sol)

  implicit none

  real(8) matr(*),br(*),sol(*)

  integer (4) n

  real(8) piv
  integer(4) i,ii

  real (8),allocatable :: mat(:),b(:)

allocate (mat(3*n-2))
mat(1:3*n-2) = matr(1:3*n-2)
allocate (b(n))
b(1:n) = br(1:n)
  !
  ! descente

  do i=2,n-1
     ii = 3*(i-1)
     piv = mat(ii)/mat(ii-1)
     mat(ii+2) = mat(ii+2)-mat(ii-2)*piv
     b(i) = b(i) - b(i-1)*piv
  enddo
!
! la derniere equation n'a pas de terme de droite
     ii = 3*(n-1)
     piv = mat(ii)/mat(ii-1)
     mat(ii+1) = mat(ii+1)-mat(ii-2)*piv
     b(i) = b(i) - b(i-1)*piv

  !
  ! remontee

  sol(n) = b(n)/mat(3*n-2)
  do i=n-1,1,-1
     sol(i) = (b(i)-mat(3*i-2)*sol(i+1))/mat (3*i-1)
  enddo
deallocate  (mat)
deallocate (b)

end subroutine gauss1d
