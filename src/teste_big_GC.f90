!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: teste_big_GC.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

real(8),allocatable :: mat6(:),b(:),sol(:)
integer(4),allocatable :: mat5(:),mat4(:)

real(8) epsgc
integer(4) deg_max_gc

include 'dump.h'

idumps(:) = 0
idump_solgc = 2

open (unit=1,file='systeme',form='unformatted')
read (1) ndl;write (6,*) ' ndl =',ndl
read (1) lbid;write (6,*) ' lbid =',lbid

allocate (mat4(ndl+1))
read (1) mat4
lmat5 = mat4(ndl+1); write (6,*) ' lmat5 =',lmat5
allocate (mat5(lmat5))
read (1) mat5
allocate (mat6(lmat5))
read (1) mat6
allocate (b(ndl))
read (1) b
close(1)
allocate (sol(ndl))

call GC_init_sys (mat4,mat5,ndl)

!lmat5 = 10
!ndl = 4
nsys = 1
initb0 = 0
nivprop = 3
lmac5prop = 0
niter = 200
itrait = 1
istgrc = 1
i_cal_pc = 1
ieco = 0
epsgc = 1.d-9
deg_max_gc = 10
call GC_solve (mat6,lmat5,b,ndl,nsys,sol,sol,initb0,nivprop,lmac5prop,niter,itrait,istgrc,i_cal_pc,ieco,epsgc,deg_max_gc,message)

open (unit=1,file='solution')
write (1,*) sol
close (1)

end

