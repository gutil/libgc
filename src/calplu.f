      SUBROUTINE CALPLU(NTDL,NIVEAU,LONG,MAT4,MAT5,
     S                  MAC4,MAC5,MAC7,tlmac5,iappli)
        include 'dprec.h'
        include 'dump.h'
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C                    S.P. CALPLU
C                    -----------
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C BUT : CE SP CALCULE LES POINTEURS DE LA MATRICE DE
C ----  PRECONDITIONNEMENT ASSOCIEE A LA FACTORISATION DE
C       GAUSS INCOMPLETE DE A SUIVANT LA NOTION DE NIVEAU
C
C   PARAMETRES D'ENTREE:
C   -------------------
C   NTDL      : LE RANG DU SYSTEME
C   NIVEAU    : PARAMETRE DE FACTORISATION INCOMPLETE
C   LONG      : NOMBRE MAXIMUM DE COEFFICIENTS DE MAC6
C   MAT4,MAT5 : LES POINTEURS ASSOCIES
c   tlmac5    : tableau des tailles de mac5 en fonction du niveau
C
C   PARAMETRE DE SORTIE:
C   -------------------
C   MAC4,MAC5,MAC7 : LES POINTEURS ASSOCIES
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C   PROGRAMMEUR : P.JOLY  LABORATOIRE D'ANALYSE NUMERIQUE  PARIS 6
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      DIMENSION MAT4(*),MAT5(*)
      DIMENSION MAC4(*),MAC7(NTDL)
!     DIMENSION MAC7(NTDL)
      integer(4),pointer :: mac5(:)
      integer(4) tlmac5(0:10,*)
      COMMON /UNITES/ LECTEU,IMPRIM,FILUNI(30)
      COMMON/TRAVA1/NX(29),IMPRE,IFILL(3)
      include 'entsor.h'

      interface
	subroutine caplu1 (ntdl,mac4,mac5,mac7,lmac5)
	integer(4) :: ntdl,mac4(*),mac7(*),
     1                lmac5
	integer(4),pointer :: mac5(:)
	end subroutine caplu1
      end interface

C
C     INITIALISATION DES POINTEURS
C     ----------------------------
C

cwrite (imp,*) ' calplu...',long

    !  IMPRE = 4 



      IMPR1=IABS(IMPRE)
      DO 1 I=1,NTDL+1
      MAC4(I)=MAT4(I)
1     CONTINUE
!
! si la taille de mac5 pour le niveau est deja connue, on l'utilise
!
      lmac5 = tlmac5(niveau,iappli)
      if (lmac5 .eq. 0) lmac5 = mat4(ntdl+1)
      if (associated(mac5) .and. size (mac5) .ne. lmac5) then
         deallocate (mac5)
      endif
      if (.not. associated(mac5)) then
         if (idump_solgc .gt. 1)
     1   write (imp,*) ' Allocation de mac5, dimension',lmac5
         allocate (mac5(lmac5),stat=ires)
         call diag_alloc(ires,' mac5 (calplu)')
      endif
      long = lmac5
      DO 2 I=1,MAT4(NTDL+1)
      MAC5(I)=MAT5(I)
2     CONTINUE
      CALL CAPLU2(NTDL,MAC4,MAC5,MAC7)
C
C     BOUCLE SUR LE NIVEAU
C     --------------------
C
      DO 3 NIV=1,NIVEAU
	if (idump_solgc .gt. 1)
     1   Write (imprim,*) ' Niveau (calplu)',niv
      CALL CAPLU1(NTDL,MAC4,MAC5,MAC7,long)
      CALL CAPLU2(NTDL,MAC4,MAC5,MAC7)
      IF(MAC4(NTDL+1).GT.LONG) THEN
c
c message supprime <= redimensionnement automatique
c     WRITE(IMPRIM,2000) MAC4(NTDL+1),LONG
c modif pour pouvoir iterer en augmentant la dimension
c     STOP
      long = mac4(ntdl+1)
c
c on sort de la boucle sur les niveaux
	goto 4
      END IF
3     CONTINUE
4     continue
C
C     IMPRESSIONS
C     -----------
C
      IF(IMPR1.GE.2) THEN
      WRITE(IMPRIM,1000) NTDL,MAT4(NTDL+1),NIVEAU,MAC4(NTDL+1)
      END IF
C
      RETURN
1000  FORMAT(' NOMBRE D''INCONNUES',T41,I8/,
     S       ' NOMBRE DE COEFFICIENTS NON NULS DE A ',T41,I8/,
     S       ' FACTORISATION INCOMPLETE DE NIVEAU',T41,I8/,
     S       ' NOMBRE DE COEFFICIENTS NON NULS DE CA',T41,I8)
2000  FORMAT(' NOMBRE DE COEFFICIENTS NON NULS DE CA',T41,I8/,
     S       ' SUPERIEUR A LA LIMITE AUTORISEE      ',T41,I8/
     s       ' ajuster le parametre lmac5 de la carte de donnees',
     s       ' "Gradient Conjugue"'/)
      END
