	subroutine addpref (nom)

        use macros_mod

	include 'prefixe.h'
        include 'entsor.h'


	character*(*) nom
	character*256 newnom,nom_macro,macro


        newnom(:) = ' '
!       
!       nom commencant par un . : on ajoute le prefixe du job
!       
	if (nom(1:1) .eq. '.') then

!       
!       nom de job comme prefixe (le nom commence par .)
!       
	   if (nom_job(1:10) .eq. "          ") return
	   newnom = trim (nom_job)
	   l = len_trim (nom_job)
!       
!       on ajoute le nom, en supprimant le point s'il etait suivi immediatement d'une
!       macro (l'utilisateur met lui-meme le point dans la macro)
	   if (nom(2:2) .eq. '$') then
	      write (newnom(l+1:),fmt='(a)') trim(nom(2:))
	   else
	      write (newnom(l+1:),fmt='(a)') trim(nom)
	   endif

	   l = len_trim (newnom)
	   nom(1:l) = newnom(1:l)
!         write (6,*) ' nom (2)', nom(1:l)
	endif
!       
!       macro comme prefixe
!       
        imac = index (nom,'$')

!       if (nom(1:1) .eq. '$') then
        if (imac .ne. 0) then
!       
!       decodage de la macro
!       
           i = index (nom(imac:),'.')
!       
           if (i .eq. 0) then
!       
!       macro en fin de nom
              i = len_trim(nom)+1
           else
              i = i+imac-1
           endif
           nom_macro = nom(imac+1:i-1)
           call get_macro (nom_macro,macro,l)
!       
!       insertion de la macro dans le nom
!       
!       section precedent la macro
           newnom (1:imac-1) = (nom(1:imac-1))
!       
!       macro
           newnom (imac:imac+l-1) = macro(1:l)
!       
!       point: l'utilisateur doit le mettre dans la macro
!       newnom (imac+l:imac+l) = '.'
!       
!       fin du nom
           write (newnom(imac+l:256),fmt='(a)') trim(nom(i+1:))
	   nom = trim(newnom)
!
! on ne sort pas pour ajouter si necessaire le nom de job
!       return
        endif
	end
