!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: allonge_tab_int.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

subroutine allonge_tab_int (t,newl,res)

  implicit none

  integer(4),pointer :: t(:)
  integer(4),pointer :: tt(:)
  integer res,newl,i,l,lec,imp,j,ii
  common /entsor/ lec,imp

  allocate (tt(newl),stat=res)
  if (res .ne. 0) then
     write (imp,'(a)') 'Impossible d''allouer un tableau d'' entiers'
     !      call sortie ('allonge_tab_int')
     return
  endif

!
! si t existait deja, on le recopie

  if (associated(t)) then
     l = size (t)
     do i=1,l
        tt(i) = t(i)
     enddo
     deallocate (t)
  endif

  t => tt

end subroutine allonge_tab_int
