!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: espace_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module espace_mod

  include 'espace_int.h'

  integer(4), parameter :: MAX_TAB = 200
  integer(4) :: nb_tab = 0

  type desc_tab

     real (8), pointer :: ptr(:)
     integer (4), pointer :: pte(:)
     logical, pointer :: ptl(:)
     real (8), pointer :: ptr2(:,:)
     integer (4), pointer :: pte2(:,:)
     character (30) :: nom
     character (30) :: nom_int
     logical :: b_util
     logical :: b_present
     logical :: b_stocke
     integer(4) :: dim
     integer(4) :: dim1,dim2
     character (2) :: typ

  end type desc_tab

  type (desc_tab) :: tab_desc(MAX_TAB)

  logical b_eco_mem

contains

  subroutine enregistre_int (ptr, nom, dim)

    implicit none

    integer(4), pointer :: ptr(:)
    character*(*), intent(in) :: nom
    integer(4), intent(in) :: dim

    character(30) noml,nomc
    integer(4) num,l,i,ii

    include 'entsor.h'
    include 'dump.h'

    nomc = adjustl(nom)
    !
    ! affectation du nom interne
    !
    noml = '.'//getNom(nom)
    !
    ! creation du tableau s'il n'existe pas deja
    !
    !     write (imp,'(a,a)') 'noml = ',trim(noml)
    l = len_trim (noml) - 1
    !     write (imp,*) ' longueur du nom',trim(nom),' ',l
    ii = 0
    do i=1,nb_tab
       if (tab_desc(i)%nom(1:l) .eq. nomc(1:l)) then
          ii = i
       endif
    enddo
    if (ii .eq. 0) then
       nb_tab = nb_tab + 1
       if (nb_tab .gt. MAX_TAB) then
	  write (imp,'(///a//)')  'Augmenter MAX_TAB dans espace_mod.f90'
	  call sortie ('enregistre_int')
       endif
       ii = nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) ' Enregistrement de ',trim(nom),' longueur',dim
    endif
    tab_desc(ii)%nom = trim(nomc(1:l))
    tab_desc(ii)%dim = dim
    tab_desc(ii)%pte => ptr
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.
    tab_desc(ii)%typ = 'e1'
    tab_desc(ii)%nom_int = trim(noml)
!
! premiere creation
!
    if (ii .eq. nb_tab) then
       tab_desc(ii)%b_stocke = .FALSE.
    endif

    if (idump_espace_mod .gt. 0) then
       if (ii .eq. nb_tab) then
          write (imp,*) ' enregistrement du tableau ',trim(noml),' (',trim(nom),')'
       else
          write (imp,*) ' mise a jour du tableau ',trim(noml),' (',trim(nom),')'
       endif
    endif

  end subroutine enregistre_int

  subroutine enregistre_int_2D (ptr, nom, dim1, dim2)

    implicit none

    integer(4), pointer :: ptr(:,:)
    character*(*), intent(in) :: nom
    integer(4), intent(in) :: dim1,dim2

    character(30) noml,nomc
    integer(4) num,l,i,ii

    include 'entsor.h'
    include 'dump.h'

    nomc = adjustl(nom)
    !
    ! affectation du nom interne
    !
    noml = '.'//getNom(nom)
    !
    ! creation du tableau s'il n'existe pas deja
    !
    !     write (imp,'(a,a)') 'noml = ',trim(noml)
    l = len_trim (noml) - 1
    !     write (imp,*) ' longueur du nom',trim(nom),' ',l
    ii = 0
    do i=1,nb_tab
       if (tab_desc(i)%nom(1:l) .eq. nomc(1:l)) then
          ii = i
       endif
    enddo
    if (ii .eq. 0) then
       nb_tab = nb_tab + 1
       if (nb_tab .gt. MAX_TAB) then
	  write (imp,'(///a//)')  'Augmenter MAX_TAB dans espace_mod.f90'
	  call sortie ('enregistre_int_2D')
       endif
       ii = nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) ' Enregistrement de ',trim(nom),' longueur',dim1,' X',dim2,' =',dim1*dim2
    endif
    tab_desc(ii)%nom = trim(nomc(1:l))
    tab_desc(ii)%dim1 = dim1
    tab_desc(ii)%dim2 = dim2
    tab_desc(ii)%dim = dim1*dim2
    tab_desc(ii)%pte2 => ptr
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.
    tab_desc(ii)%typ = 'e2'
    tab_desc(ii)%nom_int = trim(noml)
!
! premiere creation
!
    if (ii .eq. nb_tab) then
       tab_desc(ii)%b_stocke = .FALSE.
    endif

    if (idump_espace_mod .gt. 0) then
       if (ii .eq. nb_tab) then
          write (imp,*) ' enregistrement du tableau ',trim(noml),' (',trim(nom),')'
       else
          write (imp,*) ' mise a jour du tableau ',trim(noml),' (',trim(nom),')'
       endif
    endif

  end subroutine enregistre_int_2D

  subroutine enregistre_flt (ptr, nom, dim)

    implicit none

    real(8), pointer :: ptr(:)
    character*(*), intent(in) :: nom
    integer(4), intent(in) :: dim

    character(30) noml,nomc
    integer(4) num,l,i,ii

    include 'entsor.h'
    include 'dump.h'

    nomc = adjustl(nom)
    !
    ! affectation du nom interne
    !
    !write (imp,'(a,a)') 'nom =',trim(nom)
    noml = '.'//getNom(nom)
    !
    ! creation du tableau s'il n'existe pas deja
    !
    !write (imp,'(a,a)') 'noml = ',trim(noml)
    l = len_trim (noml) - 1
    !write (imp,*) ' longueur du nom',trim(nom),' ',l
    ii = 0
    do i=1,nb_tab
       !write (imp,*) ' i =',i
       if (tab_desc(i)%nom(1:l) .eq. nomc(1:l)) then
          ii = i
          exit
       endif
    enddo
    if (ii .eq. 0) then
       nb_tab = nb_tab + 1
       if (nb_tab .gt. MAX_TAB) then
	  write (imp,'(///a//)')  'Augmenter MAX_TAB dans espace_mod.f90'
	  call sortie ('enregistre_flt')
       endif
       ii = nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) ' Enregistrement de ',trim(nom),' longueur',dim
    endif
    tab_desc(ii)%nom = trim(nomc(1:l))
    tab_desc(ii)%dim = dim
    tab_desc(ii)%ptr => ptr
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.
    tab_desc(ii)%typ = 'r1'
    tab_desc(ii)%nom_int = trim(noml)
!
! premiere creation
!
    if (ii .eq. nb_tab) then
       tab_desc(ii)%b_stocke = .FALSE.
    endif

    if (idump_espace_mod .gt. 0) then
       if (ii .eq. nb_tab) then
          write (imp,*) ' enregistrement du tableau ',trim(noml),' (',trim(nom),')'
       else
          write (imp,*) ' mise a jour du tableau ',trim(noml),' (',trim(nom),')'
       endif
    endif

  end subroutine enregistre_flt

  subroutine enregistre_flt_2D (ptr, nom, dim1, dim2)

    implicit none

    real(8), pointer :: ptr(:,:)
    character*(*), intent(in) :: nom
    integer(4), intent(in) :: dim1,dim2

    character(30) noml,nomc
    integer(4) num,l,i,ii

    include 'entsor.h'
    include 'dump.h'

    nomc = adjustl(nom)
    !
    ! affectation du nom interne
    !
    noml = '.'//getNom(nom)
    !
    ! creation du tableau s'il n'existe pas deja
    !
    !     write (imp,'(a,a)') 'noml = ',trim(noml)
    l = len_trim (noml) - 1
    !     write (imp,*) ' longueur du nom',trim(nom),' ',l
    ii = 0
    do i=1,nb_tab
       if (tab_desc(i)%nom(1:l) .eq. nomc(1:l)) then
          ii = i
       endif
    enddo
    if (ii .eq. 0) then
       nb_tab = nb_tab + 1
       if (nb_tab .gt. MAX_TAB) then
	  write (imp,'(///a//)')  'Augmenter MAX_TAB dans espace_mod.f90'
	  call sortie ('enregistre_flt_2D')
       endif
       ii = nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) ' Enregistrement de ',trim(nom),' longueur',dim1,' X',dim2,' =',dim1*dim2
    endif
    tab_desc(ii)%nom = trim(nomc(1:l))
    tab_desc(ii)%dim1 = dim1
    tab_desc(ii)%dim2 = dim2
    tab_desc(ii)%dim = dim1*dim2
    tab_desc(ii)%ptr2 => ptr
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.
    tab_desc(ii)%typ = 'r2'
    tab_desc(ii)%nom_int = trim(noml)
!
! premiere creation
!
    if (ii .eq. nb_tab) then
       tab_desc(ii)%b_stocke = .FALSE.
    endif

    if (idump_espace_mod .gt. 0) then
       if (ii .eq. nb_tab) then
          write (imp,*) ' enregistrement du tableau ',trim(noml),' (',trim(nom),')'
       else
          write (imp,*) ' mise a jour du tableau ',trim(noml),' (',trim(nom),')'
       endif
    endif

  end subroutine enregistre_flt_2D

  subroutine enregistre_log (ptr, nom, dim)

    implicit none

    logical, pointer :: ptr(:)
    character*(*), intent(in) :: nom
    integer(4), intent(in) :: dim

    character(30) noml,nomc
    integer(4) num,l,i,ii

    include 'entsor.h'
    include 'dump.h'

    nomc = adjustl(nom)
    !
    ! affectation du nom interne
    !
    noml = '.'//getNom(nom)
    !
    ! creation du tableau s'il n'existe pas deja
    !
    !     write (imp,'(a,a)') 'noml = ',trim(noml)
    l = len_trim (noml) - 1
    !     write (imp,*) ' longueur du nom',trim(nom),' ',l
    ii = 0
    do i=1,nb_tab
       if (tab_desc(i)%nom(1:l) .eq. nomc(1:l)) then
          ii = i
       endif
    enddo
    if (ii .eq. 0) then
       nb_tab = nb_tab + 1
       if (nb_tab .gt. MAX_TAB) then
	  write (imp,'(///a//)')  'Augmenter MAX_TAB dans espace_mod.f90'
	  call sortie ('enregistre_log')
       endif
       ii = nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) ' Enregistrement de ',trim(nom),' longueur',dim
    endif
    tab_desc(ii)%nom = trim(nomc(1:l))
    tab_desc(ii)%dim = dim
    tab_desc(ii)%ptl => ptr
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.
    tab_desc(ii)%typ = 'e1'
    tab_desc(ii)%nom_int = trim(noml)
!
! premiere creation
!
    if (ii .eq. nb_tab) then
       tab_desc(ii)%b_stocke = .FALSE.
    endif

    if (idump_espace_mod .gt. 0) then
       if (ii .eq. nb_tab) then
          write (imp,*) ' enregistrement du tableau ',trim(noml),' (',trim(nom),')'
       else
          write (imp,*) ' mise a jour du tableau ',trim(noml),' (',trim(nom),')'
       endif
    endif

  end subroutine enregistre_log

  subroutine desenregistre_int (tab)

    implicit none

    integer(4),pointer :: tab(:)
    integer(4) i,ii

    ii = 0
    do i=1,nb_tab
       if (associated(tab_desc(i)%pte,tab)) then
          ii = i
          !write (6,*) ' je desenregistre ',trim(tab_desc(i)%nom_int)
       endif
    enddo
    if (ii .ne. 0) then
       call sup_tab (ii)
    endif

  end subroutine desenregistre_int

  subroutine desenregistre_flt (tab)

    implicit none

    real(8),pointer :: tab(:)
    integer(4) i,ii

    !call liste_tab

    ii = 0
    do i=1,nb_tab
       if (associated(tab_desc(i)%ptr,tab)) then
	  ii = i
          !write (6,*) ' je desenregistre ',trim(tab_desc(i)%nom_int)
       endif
    enddo
    if (ii .ne. 0) then
       call sup_tab (ii)
    endif

    !call liste_tab

  end subroutine desenregistre_flt

  subroutine desenregistre_int_2D (tab)

    implicit none

    integer(4),pointer :: tab(:,:)
    integer(4) i,ii

    ii = 0
    do i=1,nb_tab
       ii = i
       if (associated(tab_desc(i)%pte2,tab)) then
          !write (6,*) ' je desenregistre ',trim(tab_desc(i)%nom_int)
       endif
    enddo
    if (ii .ne. 0) then
       call sup_tab (ii)
    endif

  end subroutine desenregistre_int_2D

  subroutine desenregistre_flt_2D (tab)

    implicit none

    real(8),pointer :: tab(:,:)
    integer(4) i,ii

    ii = 0
    do i=1,nb_tab
       if (associated(tab_desc(i)%ptr2,tab)) then
          ii = i
          !write (6,*) ' je desenregistre ',trim(tab_desc(i)%nom_int)
       endif
    enddo
    if (ii .ne. 0) then
       call sup_tab (ii)
    endif

  end subroutine desenregistre_flt_2D

  subroutine desenregistre_log (tab)

    implicit none

    logical,pointer :: tab(:)
    integer(4) i,ii

    ii = 0
    do i=1,nb_tab
       if (associated(tab_desc(i)%ptl,tab)) then
          ii = i
          !write (6,*) ' je desenregistre ',trim(tab_desc(i)%nom_int)
       endif
    enddo
    if (ii .ne. 0) then
       call sup_tab (ii)
    endif

  end subroutine desenregistre_log

  function getNom (nom)

    implicit none

    character(*), intent(in) :: nom

    character(30) :: getNom,s

    integer(4) :: l

    include 'entsor.h'
    include 'dump.h'

    s = adjustl(nom)

    l = index (s,' ')

    if (l .eq. 0) then
       getNom = s
    else
       getNom = s (1:l-1)
    endif

  end function getNom

  subroutine place_nette

    implicit none

    integer(4) :: i
    integer(8) lib

    include 'entsor.h'
    include 'dump.h'

    !     include 'espace_int.h'

    if (idump_espace_mod .gt. 0) write (imp,'(//a/)') ' Liste des tableaux disponibles :'
    lib = 0
    do i=1,nb_tab
       if (idump_espace_mod .gt. 0) write (imp,*) trim (tab_desc(i)%nom),' ',trim (tab_desc(i)%nom_int)
       if (tab_desc(i)%b_util .eqv. .FALSE.) then
          if (idump_espace_mod .gt. 0) write (imp,*) '... a sauvegarder'
          if (tab_desc(i)%typ .eq. 'e1') then
             call sauve_tab_int (tab_desc(i)%pte)
	     lib = lib + size(tab_desc(i)%pte)*4
             deallocate (tab_desc(i)%pte)
          elseif (tab_desc(i)%typ .eq. 'e2') then
             call sauve_tab_int_2D (tab_desc(i)%pte2)
	     lib = lib + size(tab_desc(i)%pte2)*4
             deallocate (tab_desc(i)%pte2)
          elseif (tab_desc(i)%typ .eq. 'r1') then
             call sauve_tab_flt (tab_desc(i)%ptr)
	     lib = lib + size(tab_desc(i)%ptr)*8
             deallocate (tab_desc(i)%ptr)
          elseif (tab_desc(i)%typ .eq. 'r2') then
             call sauve_tab_flt_2D (tab_desc(i)%ptr2)
	     lib = lib + size(tab_desc(i)%ptr2)*8
             deallocate (tab_desc(i)%ptr2)
          endif
          tab_desc(i)%b_present = .FALSE.
          tab_desc(i)%b_stocke = .TRUE.
       endif
    enddo
    write (imp,*) ' Espace libere',lib

  end subroutine place_nette

  subroutine libere_int (ptr)

    implicit none

    integer(4),pointer :: ptr(:)
    integer(4) :: i
    include 'entsor.h'
    include 'dump.h'

    if (associated (ptr)) then

       do i=1,nb_tab

          if (associated(tab_desc(i)%pte,ptr)) then
             if (idump_espace_mod .gt. 0) write (imp,*) ' liberation du tableau ',tab_desc(i)%nom
             tab_desc(i)%b_util = .FALSE.
             exit
          endif

       enddo

    endif

  end subroutine libere_int

  subroutine libere_int_2D (ptr)

    implicit none

    integer(4),pointer :: ptr(:,:)
    integer(4) :: i
    include 'entsor.h'
    include 'dump.h'

    if (associated (ptr)) then

       do i=1,nb_tab

          if (associated(tab_desc(i)%pte2,ptr)) then
             if (idump_espace_mod .gt. 0) write (imp,*) ' liberation du tableau ',tab_desc(i)%nom
             tab_desc(i)%b_util = .FALSE.
             exit
          endif

       enddo

    endif

  end subroutine libere_int_2D

  subroutine libere_flt (ptr)

    implicit none

    real(8),pointer :: ptr(:)
    integer(4) :: i
    include 'entsor.h'
    include 'dump.h'

    if (associated (ptr)) then

       do i=1,nb_tab

          if (associated(tab_desc(i)%ptr,ptr)) then
             if (idump_espace_mod .gt. 0) write (imp,*) ' liberation du tableau ',tab_desc(i)%nom
             tab_desc(i)%b_util = .FALSE.
             exit
          endif

       enddo

    endif

  end subroutine libere_flt

  subroutine libere_flt_2D (ptr)

    implicit none

    real(8),pointer :: ptr(:,:)
    integer(4) :: i
    include 'entsor.h'
    include 'dump.h'

    if (associated (ptr)) then

       do i=1,nb_tab

          if (associated(tab_desc(i)%ptr2,ptr)) then
             if (idump_espace_mod .gt. 0) write (imp,*) ' liberation du tableau ',tab_desc(i)%nom
             tab_desc(i)%b_util = .FALSE.
             exit
          endif

       enddo

    endif

  end subroutine libere_flt_2D

  subroutine libere_log (ptr)

    implicit none

    logical,pointer :: ptr(:)
    integer(4) :: i
    include 'entsor.h'
    include 'dump.h'

    if (associated (ptr)) then

       do i=1,nb_tab

          if (associated(tab_desc(i)%ptl,ptr)) then
             if (idump_espace_mod .gt. 0) write (imp,*) ' liberation du tableau ',tab_desc(i)%nom
             tab_desc(i)%b_util = .FALSE.
             exit
          endif

       enddo

    endif

  end subroutine libere_log

  subroutine sauve_tab_int (ptr)

    use fichier_mod

    implicit none

    integer(4),pointer :: ptr(:)
    integer(4) :: i,ii,ul,iopen,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (associated (ptr)) then

       ii = 0
       do i=1,nb_tab

          if (associated(tab_desc(i)%pte,ptr)) then
             ii = i
             exit
          endif

       enddo
       if (ii .eq. 0) then
          write (imp,'(//a/a//)') ' Anomalie detectee dans sauve_tab_int',' Tableau non trouve dans la liste'
          call sortie ('sauve_tab_int')
       endif

       if (tab_desc(ii)%b_util .neqv. .FALSE.) then
          write (imp,'(/a,a/a)') ' Impossible de sauver le tableau',tab_desc(ii)%nom,' Ce tableau n''a pas ete libere'
          call sortie ('sauve_tab_int')
       endif

       !
       ! ecriture sur disque
       !
       nomfic = trim(tab_desc(ii)%nom_int)
       call addpref (nomfic)
       nomfic = trim(nomfic)//'_'
       if (idump_espace_mod .gt. 0) write (imp,*) '      on ecrit le fichier ',trim(nomfic)
       ul = 1 !50
       call connecte (nomfic,ul,iopen,'nfor')
!      write (ul) (tab_desc(ii)%pte(i),i=1,tab_desc(ii)%dim)
       long = tab_desc(ii)%dim
       write (ul) (ptr(i),i=1,long)
       close (ul)
       call free_ul (ul)

    endif

  end subroutine sauve_tab_int

  subroutine sauve_tab_int_2D (ptr)

    use fichier_mod

    implicit none

    integer(4),pointer :: ptr(:,:)
    integer(4) :: i,ii,ul,iopen,dim1,dim2,j

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (associated (ptr)) then

       ii = 0
       do i=1,nb_tab

          if (associated(tab_desc(i)%pte2,ptr)) then
             ii = i
             exit
          endif

       enddo
       if (ii .eq. 0) then
          write (imp,'(//a/a//)') ' Anomalie detectee dans sauve_tab_int_2D',' Tableau non trouve dans la liste'
          call sortie ('sauve_tab_int_2D')
       endif

       if (tab_desc(ii)%b_util .neqv. .FALSE.) then
          write (imp,'(/a,a/a)') ' Impossible de sauver le tableau',tab_desc(ii)%nom,' Ce tableau n''a pas ete libere'
          call sortie ('sauve_tab_int_2D')
       endif

       !
       ! ecriture sur disque
       !
       nomfic = trim(tab_desc(ii)%nom_int)
       call addpref (nomfic)
       nomfic = trim(nomfic)//'_'
       if (idump_espace_mod .gt. 0) write (imp,*) '      on ecrit le fichier ',trim(nomfic)
       ul = 1 !50
       call connecte (nomfic,ul,iopen,'nfor')
       dim1 = size (tab_desc(ii)%pte2,1)
       dim2 = size (tab_desc(ii)%pte2,2)
!      write (ul) ((tab_desc(ii)%pte2(i,j),i=1,dim1),j=1,dim2)
       write (ul) ((ptr(i,j),i=1,dim1),j=1,dim2)
       close (ul)
       call free_ul (ul)

    endif

  end subroutine sauve_tab_int_2D

  subroutine sauve_tab_flt (ptr)

    use fichier_mod

    implicit none

    real(8),pointer :: ptr(:)
    integer(4) :: i,ii,ul,iopen,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (associated (ptr)) then

       ii = 0
       do i=1,nb_tab

          if (associated(tab_desc(i)%ptr,ptr)) then
             ii = i
             exit
          endif

       enddo
       if (ii .eq. 0) then
          write (imp,'(//a/a//)') ' Anomalie detectee dans sauve_tab_flt',' Tableau non trouve dans la liste'
          call sortie ('sauve_tab_flt')
       endif

       if (tab_desc(ii)%b_util .neqv. .FALSE.) then
          write (imp,'(/a,a/a)') ' Impossible de sauver le tableau',tab_desc(ii)%nom,' Ce tableau n''a pas ete libere'
          call sortie ('sauve_tab_flt')
       endif

       !
       ! ecriture sur disque
       !
       nomfic = trim(tab_desc(ii)%nom_int)
       call addpref (nomfic)
       nomfic = trim(nomfic)//'_'
       if (idump_espace_mod .gt. 0) write (imp,*) '      on ecrit le fichier ',trim(nomfic)
       ul = 1 !50
       call connecte (nomfic,ul,iopen,'nfor')
!      write (ul) (tab_desc(ii)%ptr(i),i=1,tab_desc(ii)%dim)
       long = tab_desc(ii)%dim
       write (ul) (ptr(i),i=1,long)
       close (ul)
       call free_ul (ul)

    endif

  end subroutine sauve_tab_flt

  subroutine sauve_tab_flt_2D (ptr)

    use fichier_mod

    implicit none

    real(8),pointer :: ptr(:,:)
    integer(4) :: i,ii,ul,iopen,dim1,dim2,j

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (associated (ptr)) then

       ii = 0
       do i=1,nb_tab

          if (associated(tab_desc(i)%ptr2,ptr)) then
             ii = i
             exit
          endif

       enddo
       if (ii .eq. 0) then
          write (imp,'(//a/a//)') ' Anomalie detectee dans sauve_tab_flt_2D',' Tableau non trouve dans la liste'
          call sortie ('sauve_tab_flt_2D')
       endif

       if (tab_desc(ii)%b_util .neqv. .FALSE.) then
          write (imp,'(/a,a/a)') ' Impossible de sauver le tableau',tab_desc(ii)%nom,' Ce tableau n''a pas ete libere'
          call sortie ('sauve_tab_flt_2D')
       endif

       !
       ! ecriture sur disque
       !
       nomfic = trim(tab_desc(ii)%nom_int)
       call addpref (nomfic)
       nomfic = trim(nomfic)//'_'
       if (idump_espace_mod .gt. 0) write (imp,*) '      on ecrit le fichier ',trim(nomfic)
       ul = 1 !50
       call connecte (nomfic,ul,iopen,'nfor')
       dim1 = size (tab_desc(ii)%ptr2,1)
       dim2 = size (tab_desc(ii)%ptr2,2)
!      write (ul) ((tab_desc(ii)%ptr2(i,j),i=1,dim1),j=1,dim2)
       write (ul) ((ptr(i,j),i=1,dim1),j=1,dim2)
       close (ul)
       call free_ul (ul)

    endif

  end subroutine sauve_tab_flt_2D

  subroutine sauve_tab_log (ptr)

    use fichier_mod

    implicit none

    logical,pointer :: ptr(:)
    integer(4) :: i,ii,ul,iopen,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (associated (ptr)) then

       ii = 0
       do i=1,nb_tab

          if (associated(tab_desc(i)%ptl,ptr)) then
             ii = i
             exit
          endif

       enddo
       if (ii .eq. 0) then
          write (imp,'(//a/a//)') ' Anomalie detectee dans sauve_tab_int',' Tableau non trouve dans la liste'
          call sortie ('sauve_tab_log')
       endif

       if (tab_desc(ii)%b_util .neqv. .FALSE.) then
          write (imp,'(/a,a/a)') ' Impossible de sauver le tableau',tab_desc(ii)%nom,' Ce tableau n''a pas ete libere'
          call sortie ('sauve_tab_log')
       endif

       !
       ! ecriture sur disque
       !
       nomfic = trim(tab_desc(ii)%nom_int)
       call addpref (nomfic)
       nomfic = trim(nomfic)//'_'
       if (idump_espace_mod .gt. 0) write (imp,*) '      on ecrit le fichier ',trim(nomfic)
       ul = 1 !50
       call connecte (nomfic,ul,iopen,'nfor')
!      write (ul) (tab_desc(ii)%pte(i),i=1,tab_desc(ii)%dim)
       long = tab_desc(ii)%dim
       write (ul) (ptr(i),i=1,long)
       close (ul)
       call free_ul (ul)

    endif

  end subroutine sauve_tab_log

  subroutine restore_tab_int (ptr,nom)

    use fichier_mod

    implicit none

    integer(4),pointer :: ptr(:)
    character*(*) nom
    integer(4) :: i,ii,ul,iopen,res,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (idump_espace_mod .ge.1) call liste_tab ! verif
    if (idump_espace_mod .ge. 2) write (imp,*) ' on restore ',trim(nom)
    !
    ! si le tableau est enregistre, on verifie qu'il est en memoire
    !

    ii = 0
    do i=1,nb_tab

       if (idump_espace_mod .ge. 2) write (imp,*) ' serait-ce ',trim(getNom(tab_desc(i)%nom)), ' ?'
       if (trim(getNom(tab_desc(i)%nom)) .eq. trim (adjustl(nom))) then
          ii = i
          if (idump_espace_mod .ge. 2) write (imp,*) ' eh bien oui !!'
          exit
       endif

    enddo

    if (ii .eq. 0) then
       if (idump_espace_mod .gt. 0) write (imp,'(/a,a/a/)') ' Impossible de relire le tableau ',trim (adjustl(nom)),  &
            ' Ce tableau n''a pas ete enregistre'
!      call sortie ('restore_tab_int')
       return
    endif
    if (tab_desc(ii)%b_present) then
       return
    endif
    !
    ! sinon, on relit le tableau
    !
    long = tab_desc(ii)%dim
    allocate (ptr(long),stat = res)
    call diag_alloc (res,trim (adjustl(nom))//' (relit)')

    nomfic = trim(tab_desc(ii)%nom_int)
    call addpref (nomfic)
    nomfic = trim(nomfic)//'_'
    if (idump_espace_mod .gt. 0) write (imp,*) '      on relit le fichier ',trim(nomfic)
    ul = 1 !50
    call connecte (nomfic,ul,iopen,'nfor')
    read (ul) (ptr(i),i=1,long)
    close (ul)
    call free_ul (ul)
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.

  end subroutine restore_tab_int

  subroutine restore_tab_int_2D (ptr,nom)

    use fichier_mod

    implicit none

    integer(4),pointer :: ptr(:,:)
    character*(*) nom
    integer(4) :: i,j,ii,ul,iopen,res,dim1,dim2

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (idump_espace_mod .ge.1) call liste_tab ! verif
    if (idump_espace_mod .ge. 2) write (imp,*) ' on restore ',trim(nom)
    !
    ! si le tableau est enregistre, on verifie qu'il est en memoire
    !

    ii = 0
    do i=1,nb_tab

       if (idump_espace_mod .ge. 2) write (imp,*) ' serait-ce ',trim(getNom(tab_desc(i)%nom)), ' ?'
       if (trim(getNom(tab_desc(i)%nom)) .eq. trim (adjustl(nom))) then
          ii = i
          if (idump_espace_mod .ge. 2) write (imp,*) ' eh bien oui !!'
          exit
       endif

    enddo

    if (ii .eq. 0) then
       if (idump_espace_mod .gt. 0) write (imp,'(/a,a/a/)') ' Impossible de relire le tableau ',trim (adjustl(nom)),  &
            ' Ce tableau n''a pas ete enregistre'
!      call sortie ('restore_tab_int_2D')
       return
    endif
    if (tab_desc(ii)%b_present) then
       return
    endif
    !
    ! sinon, on relit le tableau
    !
    dim1 = tab_desc(ii)%dim1
    dim2 = tab_desc(ii)%dim2
    allocate (ptr(dim1,dim2),stat = res)
    call diag_alloc (res,trim (adjustl(nom))//' (relit)')

    nomfic = trim(tab_desc(ii)%nom_int)
    call addpref (nomfic)
    nomfic = trim(nomfic)//'_'
    if (idump_espace_mod .gt. 0) write (imp,*) '      on relit le fichier ',trim(nomfic)
    ul = 1 !50
    call connecte (nomfic,ul,iopen,'nfor')
    read (ul) ((ptr(i,j),i=1,dim1),j=1,dim2)
    close (ul)
    call free_ul (ul)
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.

  end subroutine restore_tab_int_2D

  subroutine restore_tab_flt (ptr,nom)

    use fichier_mod

    implicit none

    real(8),pointer :: ptr(:)
    character*(*) nom
    integer(4) :: i,ii,ul,iopen,res,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (idump_espace_mod .ge.1) call liste_tab ! verif
    if (idump_espace_mod .ge. 2) write (imp,*) ' on restore ',trim(nom)
    !
    ! si le tableau est enregistre, on verifie qu'il est en memoire
    !

    ii = 0
    do i=1,nb_tab

    if (idump_espace_mod .ge. 2) write (imp,*) ' serait-ce ',trim(getNom(tab_desc(i)%nom)), ' ?'
       if (trim(getNom(tab_desc(i)%nom)) .eq. trim (adjustl(nom))) then
          ii = i
          if (idump_espace_mod .ge. 2) write (imp,*) ' eh bien oui !!'
          exit
       endif

    enddo

    if (ii .eq. 0) then
       if (idump_espace_mod .gt. 0) write (imp,'(/a,a/a/)') ' Impossible de relire le tableau ',trim (adjustl(nom)),  &
            ' Ce tableau n''a pas ete enregistre'
!      call sortie ('restore_tab_flt')
       return
    endif

    if (tab_desc(ii)%b_present) then
       return
    endif
    !
    ! sinon, on relit le tableau
    !
    long = tab_desc(ii)%dim
    allocate (ptr(long),stat = res)
    call diag_alloc (res,trim (adjustl(nom))//' (relit)')

    nomfic = trim(tab_desc(ii)%nom_int)
    call addpref (nomfic)
    nomfic = trim(nomfic)//'_'
    if (idump_espace_mod .gt. 0) write (imp,*) '      on relit le fichier ',trim(nomfic)
    ul = 1 !50
    call connecte (nomfic,ul,iopen,'nfor')
    read (ul) (ptr(i),i=1,long)
    close (ul)
    call free_ul (ul)
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.

  end subroutine restore_tab_flt

  subroutine restore_tab_flt_2D (ptr,nom)

    use fichier_mod

    implicit none

    real(8),pointer :: ptr(:,:)
    character*(*) nom
    integer(4) :: i,j,ii,ul,iopen,res,dim1,dim2

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (idump_espace_mod .ge.1) call liste_tab ! verif
    if (idump_espace_mod .ge. 2) write (imp,*) ' on restore ',trim(nom)
    !
    ! si le tableau est enregistre, on verifie qu'il est en memoire
    !

    ii = 0
    do i=1,nb_tab

       if (idump_espace_mod .ge. 2) write (imp,*) ' serait-ce ',trim(getNom(tab_desc(i)%nom)), ' ?'
       if (trim(getNom(tab_desc(i)%nom)) .eq. trim (adjustl(nom))) then
          ii = i
          if (idump_espace_mod .ge. 2) write (imp,*) ' eh bien oui !!'
          exit
       endif

    enddo

    if (ii .eq. 0) then
       if (idump_espace_mod .gt. 0) write (imp,'(/a,a/a/)') ' Impossible de relire le tableau ',trim (adjustl(nom)),  &
            ' Ce tableau n''a pas ete enregistre'
!      call sortie ('restore_tab_flt_2D')
       return
    endif
    if (tab_desc(ii)%b_present) then
       return
    endif
    !
    ! sinon, on relit le tableau
    !
    dim1 = tab_desc(ii)%dim1
    dim2 = tab_desc(ii)%dim2
    allocate (ptr(dim1,dim2),stat = res)
    call diag_alloc (res,trim (adjustl(nom))//' (relit)')

    nomfic = trim(tab_desc(ii)%nom_int)
    call addpref (nomfic)
    nomfic = trim(nomfic)//'_'
    if (idump_espace_mod .gt. 0) write (imp,*) '      on relit le fichier ',trim(nomfic)
    ul = 1 !50
    call connecte (nomfic,ul,iopen,'nfor')
    read (ul) ((ptr(i,j),i=1,dim1),j=1,dim2)
    close (ul)
    call free_ul (ul)
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.

  end subroutine restore_tab_flt_2D

  subroutine restore_tab_log (ptr,nom)

    use fichier_mod

    implicit none

    logical,pointer :: ptr(:)
    character*(*) nom
    integer(4) :: i,ii,ul,iopen,res,long

    character(256) nomfic

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    if (idump_espace_mod .ge.1) call liste_tab ! verif
    if (idump_espace_mod .ge. 2) write (imp,*) ' on restore ',trim(nom)
    !
    ! si le tableau est enregistre, on verifie qu'il est en memoire
    !

    ii = 0
    do i=1,nb_tab

       if (idump_espace_mod .ge. 2) write (imp,*) ' serait-ce ',trim(getNom(tab_desc(i)%nom)), ' ?'
       if (trim(getNom(tab_desc(i)%nom)) .eq. trim (adjustl(nom))) then
          ii = i
          if (idump_espace_mod .ge. 2) write (imp,*) ' eh bien oui !!'
          exit
       endif

    enddo

    if (ii .eq. 0) then
       if (idump_espace_mod .gt. 0) write (imp,'(/a,a/a/)') ' Impossible de relire le tableau ',trim (adjustl(nom)),  &
            ' Ce tableau n''a pas ete enregistre'
!      call sortie ('restore_tab_log')
       return
    endif
    if (tab_desc(ii)%b_present) then
       return
    endif
    !
    ! sinon, on relit le tableau
    !
    long = tab_desc(ii)%dim
    allocate (ptr(long),stat = res)
    call diag_alloc (res,trim (adjustl(nom))//' (relit)')

    nomfic = trim(tab_desc(ii)%nom_int)
    call addpref (nomfic)
    nomfic = trim(nomfic)//'_'
    if (idump_espace_mod .gt. 0) write (imp,*) '      on relit le fichier ',trim(nomfic)
    ul = 1 !50
    call connecte (nomfic,ul,iopen,'nfor')
    read (ul) (ptr(i),i=1,long)
    close (ul)
    call free_ul (ul)
    tab_desc(ii)%b_util = .TRUE.
    tab_desc(ii)%b_present = .TRUE.

  end subroutine restore_tab_log

  subroutine menage_tab

    use fichier_mod

    implicit none

    integer(4) :: i,ii,ul,iopen

    character(256) nomfic

    logical ok

    include 'entsor.h'
    include 'dump.h'

    include 'connecte_int.h'

    do i=1,nb_tab

       if (tab_desc(i)%b_stocke) then
          nomfic = trim(tab_desc(i)%nom_int)
          call addpref (nomfic)
          nomfic = trim(nomfic)//'_'
          inquire (file=nomfic,exist=ok)
          if (ok) then
             if (idump_espace_mod .gt. 0) &
             write (imp,'(a,a)') ' Suppression de ',trim(nomfic)
             ul = 1 !50
             call connecte (nomfic,ul,iopen,'nfor')
             close (unit=ul,status='delete')
             call free_ul (ul)
          endif
       endif

    enddo

  end subroutine menage_tab

  subroutine liste_tab

    implicit none

    integer i

    include 'entsor.h'

    write (imp,*) ' Liste des tableaux'
    do i=1,nb_tab
       write (imp,'(a30,i8,l2,l2,a3,1x,a30)') tab_desc(i)%nom,tab_desc(i)%dim,tab_desc(i)%b_util,tab_desc(i)%b_present,tab_desc(i)%typ,tab_desc(i)%nom_int
    enddo

  end subroutine liste_tab

  subroutine copy_tab (i1,i2)
!
! copie de la description du tableau i1 en i2
!
     implicit none

     integer(4), intent(in) :: i1,i2

    tab_desc(i2)%nom = tab_desc(i1)%nom
    tab_desc(i2)%dim = tab_desc(i1)%dim
    if (tab_desc(i1)%typ .eq. 'e1') then
       tab_desc(i2)%pte => tab_desc(i1)%pte
    else if (tab_desc(i1)%typ .eq. 'e2') then
       tab_desc(i2)%pte2 => tab_desc(i1)%pte2
    elseif (tab_desc(i1)%typ .eq. 'r1') then
       tab_desc(i2)%ptr => tab_desc(i1)%ptr
    else
       tab_desc(i2)%ptr2 => tab_desc(i1)%ptr2
    endif
    tab_desc(i2)%b_util = tab_desc(i1)%b_util
    tab_desc(i2)%b_present = tab_desc(i1)%b_present
    tab_desc(i2)%typ = tab_desc(i1)%typ
    tab_desc(i2)%nom_int = tab_desc(i1)%nom_int

  end subroutine copy_tab

  subroutine sup_tab (ii)

    implicit none

    integer (4) :: i,ii

    include 'entsor.h'
    include 'dump.h'

    if (ii .ne. 0) then
       if (idump_espace_mod .gt. 0) write (imp,*) ' Suppression de ',trim(tab_desc(ii)%nom)
       do i=ii,nb_tab-1
          call copy_tab (i+1,i)
       enddo
       nb_tab = nb_tab - 1
    endif
   end subroutine sup_tab

end module espace_mod
