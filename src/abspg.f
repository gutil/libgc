      function abspg (arg)
c ======================================================================
c fonction abs adaptee a la simple ou la double precision
c
        include 'dprec.h'
        real rarg
        double precision darg
c        data ipass/1/
c        save ipass
c        call appels ('abspg', ipass, 1)
        if (idp.eq.1) then
           rarg = arg
           abspg = abs(rarg)
        else
           darg = arg
           abspg = dabs(darg)
        endif
c        call appels ('abspg', ipass, 2)
        end
