/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: itos.c
* 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "spmatrix.h"
#include <omp.h>
#include "libprint.h"
#include "GC.h"

// NG : 02/06/2020
char *GC_name_iappl(int iname, FILE* fpout)
{
  char *name;

  switch (iname) {
  case AQ_GC : {name=strdup("AQUIFER");break;}
  case HYD_GC : {name=strdup("RIVER NETWORK");break;}
  case TTC_GC : {name=strdup("TRANSPORT");break;}
  case HDERM_GC : {name=strdup("HYPERDERMIC NETWORK");break;}
  case TTC_AQ_GC : {name=strdup("TRANSPORT AQUIFER");break;}
  default : 
    LP_error(fpout,"In libgc%4.2f : Error in file %s, function %s : Unknown solver application number.\n",VERSION_GC,__FILE__,__func__);
  }
  return name;
}

// NG : 09/06/2020
char *GC_name_config_param(int iname, FILE* fpout)
{
  char *name;

  switch (iname) {
  case NS_GC : {name=strdup("Number of systems to be solved");break;}
  case B0_GC : {name=strdup("Initial solution provided");break;}
  case NIVPRECOND_GC : {name=strdup("Suggested preconditionning level");break;}
  case LPRECOND_GC : {name=strdup("Suggested preconditionning matrix length");break;}
  case NITMAX_GC : {name=strdup("Maximum iteration number per preconditionning level");break;}
  case TRAIT_GC : {name=strdup("Resolution status");break;}
  case STGRC_GC : {name=strdup("Stop criteria in case of non convergence");break;}
  case CALCPC_GC : {name=strdup("Preconditionning matrix calculation activation status");break;}
  case ECO_GC : {name=strdup("Preconditionning matrix precision status");break;}
  case PRECONDMAX_GC : {name=strdup("Maximum preconditionning level");break;}
  case MSG_GC : {name=strdup("Convergence criteria value");break;}
  case APPL_NB : {name=strdup("Application ID");break;}
  default : 
    LP_error(fpout,"In libgc%4.2f : Error in file %s, function %s : Unknown solver parameter.\n",VERSION_GC,__FILE__,__func__);
  }
  return name;
}


