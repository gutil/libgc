!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: macros_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module macros_mod

      character(100),pointer :: macros (:) => null()
      character(100),pointer :: nom_macros (:) => null()
      integer (4) :: n_macros = 0

contains

   subroutine add_macro (nom,macro)

      implicit none

      integer(4) :: i
      character(*) macro,nom

      include 'entsor.h'

      if (.not. associated (macros)) then
         allocate (macros(10))
         allocate (nom_macros(10))
      endif

      n_macros = n_macros + 1
      if (n_macros .gt. size(macros)) call allonge_macros
      nom_macros (n_macros) = nom
      macros (n_macros) = macro
!     write (imp,*) ' Macros disponibles:'
!     do i=1,n_macros
!        write (imp,'(a,a,a)') trim(nom_macros(i)),' : ',trim(macros(i))
!     enddo

   end subroutine add_macro

   subroutine allonge_macros

      implicit none

      integer(4) :: i
      character(100),allocatable :: prov(:),prov2(:)
      include 'entsor.h'

!     write (imp,*) ' allongement ..'
      allocate (prov(n_macros))
      allocate (prov2(n_macros))
      do i=1,n_macros
         prov(i) = nom_macros(i)
         prov2(i) = macros(i)
      enddo
      deallocate (nom_macros)
      deallocate (macros)
      allocate (nom_macros(n_macros+10))
      allocate (macros(n_macros+10))
      do i=1,n_macros
         nom_macros(i) = prov(i)
         macros(i) = prov2(i)
      enddo
      deallocate (prov)

   end subroutine allonge_macros

   subroutine get_macro (nom,macro,l)

      implicit none

      integer(4) :: i,l
      character(*) nom,macro
      include 'entsor.h'

      do i=1,n_macros
         if (trim(nom) .eq. trim(nom_macros(i))) then
            macro = trim (macros(i))
            l = len_trim(macro)
            return
         endif
      enddo

      write (imp,'(//a,a,a//)') ' Erreur : macro $',trim(nom),' non definie'
      call sortie ('get_macro')

   end subroutine get_macro

end module macros_mod
