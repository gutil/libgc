      subroutine diag_alloc (res,nom)

	   implicit none
   
	   integer res,lec,imp
	   character*(*) nom
	   common /entsor/lec,imp
	   save /entsor/

	   if (res .ne. 0) then
	      write (imp,'(//a,a//)')
     1        'Impossible d''allouer un tableau :',nom
	      call sortie ('diag_alloc')
	   endif

      end

      subroutine diag_desalloc (res,nom)

	   implicit none
   
	   integer res,lec,imp
	   character*(*) nom
	   common /entsor/lec,imp
	   save /entsor/

	   if (res .ne. 0) then
	      write (imp,'(//a,a//)')
     1        'Impossible de desallouer un tableau :',nom
	      call sortie ('diag_alloc')
	   endif

      end
