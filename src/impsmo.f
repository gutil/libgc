      subroutine impsmo (mat4,mat5,rmat6,b,nmu)
        include 'dprec.h'
	character*15 form

c
c edition d'une matrice stockee en format morse
      dimension mat4(*),mat5(*),rmat6(*),b(*)
c        data ipass/1/
c        save ipass
	common /entsor/ lec,imp

      if (nmu .lt. 10000) then
	 form = '(4(i5,e13.5))'
      else
	 form = '(4(i6,e13.5))'
      endif
      do 100 i=1,nmu
         write (imp,*) ' equation',i,mat4(i+1)-mat4(i),' termes'
         write (imp,form) (mat5(j),rmat6(j),j=mat4(i)+1,
     1   mat4(i+1))
100   continue
        write (imp,*) ' second membre'
!       write (imp,'(4(i9,e11.3))') (i,b(i),i=1,nmu)
        write (imp,'(4(i7,e13.5))') (i,b(i),i=1,nmu)
        write (imp,*) ' fin edition'

      end
