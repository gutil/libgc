c23456789012345678901234567890123456789012345678901234567890123456789012

!       subroutine GC_solve
!    1             (mat6l,lmat5,sm,ndl,nsys,h,h0,initb0,
!    2              nivprop,lmac5prop,niter,
!    3              itrait,istgrc,i_cal_pc,ieco,epsgc,deg_max_gc,
!    4              message)
        subroutine GC_solve
     1             (mat6l,lmat5,sm,ndl,h,h0,sw_int,sw_reel)

        use GC_mod

        implicit none

        integer ndl,lmat5,lmac5,itry,initb0,niveau,iok,i,nsys,niv,
     1          lmac5prop,nivprop,niter,
     2          itrait,istgrc,i_cal_pc,ieco,res_alloc,
     3          info,itn,idump,deg_max_gc,message,sw_int(*)

        real*8 sm(*),h(*),h0(*),mat6l(*),sw_reel(*)
        real*8 eps,epsgc,Anorm, Acond, rnorm, ynorm
!real(8),allocatable :: v(:),r(:),z(:),x(:),b(:)
	real(8),allocatable :: r1(:),r2(:),v(:),w(:),y(:)
	logical b_precon,b_checkA
        integer iappli

	external Aprod,Msolve

        include 'entsor.h'
        include 'dump.h'
        include 'geometrie.h'

	idump = idump_sGC

!
! recuperation des switchs

	nsys = sw_int(1)
	initb0 = sw_int(2)
	nivprop = sw_int(3)
	lmac5prop = sw_int(4)
	niter = sw_int(5)
	itrait = sw_int(6)
	istgrc = sw_int(7)
	i_cal_pc = sw_int(8)
	ieco = sw_int(9)
	deg_max_gc = sw_int(10)
	message = sw_int(11)
	epsgc = sw_reel(1)
        iappli = sw_int(12)
        wRite (6,*) ' indice d''application',iappli

	if (idump .ge. 2) then
	   call impsmo (mat4,mat5,mat6l,sm,ndl)
           write (imp,*) ' initb0, h0',initb0
           write (imp,*) h0(1:ndl)
	endif
!
! resolution directe d'une matrice 1D
	if (idim .eq. 1) then
	   call gauss1d (mat6l,sm,ndl,h)
	   return
	endif
!
! resolution par gcdiad d'une matrice symetrique
!
!	call symetrise (ndl,mat4,mat5,mat6)
!	lmat5=mat4(ndl+1)
!        allocate (v(ndl),stat=res_alloc)
!        call diag_alloc (res_alloc,'v')
!        allocate (r(ndl),stat=res_alloc)
!        call diag_alloc (res_alloc,'r')
!        allocate (z(ndl),stat=res_alloc)
!        call diag_alloc (res_alloc,'z')
!        allocate (x(ndl),stat=res_alloc)
!        call diag_alloc (res_alloc,'x')
!        allocate (b(ndl),stat=res_alloc)
!        call diag_alloc (res_alloc,'b')
!	b(1:ndl) = sm(1:ndl)
!	call gcdiad(epsgc,ndl,lmat5,1,mat4,mat5,mat6,1,
!     &                  initb0,h0,v,r,z,x,b)
!	h(1:ndl)=b(1:ndl)
!	return
!
! resolution par symmlq
!
	goto 9999
         allocate (r1(ndl),stat=res_alloc)
         call diag_alloc (res_alloc,'r1')
         allocate (r2(ndl),stat=res_alloc)
         call diag_alloc (res_alloc,'r2')
         allocate (v(ndl),stat=res_alloc)
         call diag_alloc (res_alloc,'v')
         allocate (w(ndl),stat=res_alloc)
         call diag_alloc (res_alloc,'w')
         allocate (y(ndl),stat=res_alloc)
         call diag_alloc (res_alloc,'y')

	b_precon = .TRUE.
	b_checkA = .FALSE.
	call symmlq (ndl,sm,r1,r2,v,w,h,y,Aprod,Msolve,
     &               b_checkA,.FALSE.,b_precon,0d0,6,ndl,
     &               1.d-9,info,itn,Anorm, Acond, rnorm, ynorm)
	write (imp,*) 'info =',info,' itn =',itn
	return
9999	continue

        niv = nivprop
c
c niveau de preconditionnement initial
c
c precision de la convergence

        eps = epsgc
	if (eps .eq. 0.) eps = 1.e-6

        do itry = 1,deg_max_gc

cWrite (imp,*) 'itrait =',itrait
           call app_GC (mat6l,lmat5,sm,ndl,nsys,h,h0,lmac5,
     1                  initb0,niv,eps,iok,niter,
     3 itrait,istgrc,i_cal_pc,ieco,deg_max_gc,message,iappli)

           if (iok .eq. 1) goto 1000
c
c on ne peut pas garder mac5 si on change de niveau

           itrait = 1

        enddo

1000    continue

c
c recuperation du niveau

        nivprop = niv
 
c ------------------------------------------        
c NG : 02/06/2021 : Récupération du niveau de préconditionnement 
c alors imposé comme niveau minimum pour les itérations suivantes.         
        sw_int(3) = nivprop  
c ------------------------------------------

	if (idump .ge. 1) then
	   write (imp,*) 'Solution par GC :'
	   write (imp,*) (i,h(i),i=1,ndl)
	endif

        end

        subroutine app_GC (mat6l,lmat5,sm,ndl,nsys,h,h0,lmac5,
     1                     initb0,niveau,eps,iok,niter,
     2                     itrait,istgrc,i_cal_pc,ieco,deg_max_gc,
     3                     message,iappli)

        use GC_mod
	use maillage_mod

        implicit none
 
        integer ndl,lmat5,lmac5,initb0,niveau,itrait,istgrc,niter,
     1          i_cal_pc,ndsm,message,ieco,iok,nsys,nitloc,idump
 
        real*8 sm(*),h(*),h0(*),mat6l(*)
        real*8 eps
	real*4 t1,t2

        integer res_alloc
        integer deg_max_gc
        integer iappli

        include 'entsor.h'
        include 'dump.h'

citrait = 1
cistgrc = 1
cniter = 10
ci_cal_pc = 1
        idump = idump_app_GC
        ndsm = nsys
        !message = 0
cieco = 0

c       Write (imp,'(a,i3,a,i10,a,i3)')
c    1  '       Appel de solgc avec niveau =',niveau,
c    1  ' lmac5 =',lmac5,' itrait =',itrait


        nitloc = niter

	call CPU_TIME( t1 )

        call solgc(
     3   mat6l,sm,h,h0,initb0,niveau,lmac5,ndl,eps,
     4   itrait,istgrc,nitloc,i_cal_pc,ndsm,message,ieco,deg_max_gc,
     5   iappli)

	call CPU_TIME( t2 )

	if (idump .ge. 1 .and. nitloc .ne. -1)
     1      write (imp,'(a,g12.5)') 'Temps CPU ',t2-t1

        iok = 1
        if (nitloc .eq. -1) iok = 0

        if (nitloc .ne. -1) niter = nitloc
cWrite (imp,*) 'niter,nitloc',niter,nitloc

        end
