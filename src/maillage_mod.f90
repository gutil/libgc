!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: maillage_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module maillage_mod

   integer(4),save :: nmil  ! nombre de milieux simules
   integer(4),save :: nmu0  ! nombre total de noeuds
   integer(4),save :: nbloc0! nombre total d'elements
   integer(4),save :: nmu   ! nombre total de noeuds du maillage dedouble
   integer(4),save :: nbloc ! nombre total d'elements du maillage dedouble
!  integer(4),save :: idim  ! dimension du probleme

   real(8),pointer,save :: x(:)=>null()
   real(8),pointer,save :: y(:)=>null(),z(:)=>null()
   real(8),pointer,save :: surf(:)=>null()
   real(8),pointer,save :: volel(:),volnd(:)
   logical :: b_surf_nd_connu = .FALSE.

   integer(4) :: nncont=0
   integer(4), allocatable,save :: lncont(:),jmem(:)
   integer(4), pointer,save :: nsom(:)=>null(),lis1(:)=>null(),itel(:)=>null(),numsdo(:)=>null(),nvois(:,:)=>null()
!  target :: nsom ! pour TECPLOT
   integer(4) :: nntran, nntr3d
   integer(4), pointer,save :: ndtrc(:)=>null(),ndtr1(:)=>null(),ndtr2(:)=>null(),ndt3c(:)=>null(),ndt3a(:,:)=>null()
   real(8), pointer,save :: cnt1(:)=>null(),cnt2(:)=>null()
   real(8), pointer,save :: surfel(:)=>null()
   logical :: b_surf_el_connu = .FALSE.
   real(8), allocatable,save :: surf_nd_cop(:)

contains

subroutine alloue_xyz

   use alloue_mod

   implicit none
   integer(4) res,ltot

   include 'geometrie.h'
   include 'entsor.h'


   ltot = max(idim,2)*nmu*nmil
!write (imp,*) ' alloue_xyz, idim,nmu,ltot',idim,nmu,ltot
!  allocate (x(ltot),stat=res)
!  call diag_alloc (res,' tableau x')
   call alloue (x,ltot,' x (alloue_xyz)')
   y => x(nmu*nmil+1:2*nmu*nmil)
!  allocate (y(nmu),stat=res)
!  call diag_alloc (res,' tableau y')

   if (idim .eq. 3) then
      z => x(2*nmu*nmil+1:3*nmu*nmil)
!     allocate (z(nmu),stat=res)
!     call diag_alloc (res,' tableau z')
   else
!
! sur IBM, z doit pointer vers quelque chose
      allocate (z(1))
   endif

end subroutine alloue_xyz

end module maillage_mod
