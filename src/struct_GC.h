/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libgc
* FILE NAME: struct_GC.h
* 
* CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
*               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
* 
* LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
* (https://www.netlib.org/sparse/readme (User guide available at 
* https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
* (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
* both EPL v2.0 friendly. 
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libgc Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


typedef struct  struct_grad_conjug s_gc;


struct struct_grad_conjug
{
  int    *mat4 ; //LHS and RHS id
  int    *mat5 ;
  double *mat6 ; //LHS
  double *b; //RHS
  double *x; //unknowns

  int ndl ;			// nombre d'inconnues
  int appl_nb ; // number of the apllication
  int lmat5 ;	// longueur des tableaux mat5 et mat6
  int sw_int[NINT_STRUCT_GC];//Parametrisation des fonctionnalites du solveur
  double epsgc ;		// critere de convergence
};

#define new_gc()   ((s_gc *) malloc(sizeof(s_gc)))
