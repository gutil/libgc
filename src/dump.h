
        integer idumps (300)

        integer idump_glob,idump_c3t3ens,idump_cteptns,idump_htosat,    &
     & idump_sattoh,idump_suint,idump_c3t3e,idump_c3t3d,idump_calfdi,   &
     & idump_c3h8d,idump_acmdi3,idump_inidtt,idump_cidpt,idump_ciept,   &
     & idump_inidte,idump_caldt,idump_c3t4e,idump_ctdpt,idump_vdetd,    &
     & idump_ctept,idump_ecperm,idump_sya2mo,idump_setgc,idump_setgc3d, &
     & idump_calfec,idump_trad_pot_libre,idump_starete,idump_up_t2,     &
     & idump_voisin,idump_tr_amont,idump_ch_el_amont,idump_cal_cupst,   &
     & idump_trav_source_lag,idump_int_source_lag,idump_acmdi,          &
     & idump_cagrad,idump_inter_t2d_o2,idump_inter_t2d_o3,idump_conlie, &
     & idump_interp,idump_vd3p6,idump_c3p6d,idump_acmec3,idump_acmec,   &
     & idump_c3p6e,idump_cagradel,idump_cieptns,idump_ts_ellam_tr,      &
     & idump_ts_ellam,idump_track_noeuds,idump_trclfc,idump_track_aretes&
     & ,idump_ch_tr_av,idump_setgc_ar,idump_ctept_efmh,idump_fluxec_efmh&
     & ,idump_fluxec,idump_fluxec_tri_efmh,idump_c3q4ens,idump_sGC,     &
     & idump_c3h8ens,idump_cleptns,idump_calfecns,idump_rpecns,         &
     & idump_cphetd,idump_cfecdins,idump_solgc,idump_fluxar3d,          &
     & idump_fluxec3d,idump_stface,idump_espace_mod,idump_clept,        &
     & idump_testhiter,idump_puits_psi_mod,idump_pot2pot_libre,         &
     & idump_inhibe_nds,idump_nd_from_dom,idump_masmai,idump_c3t4ens,   &
     & idump_c3s2ens,idump_c3h8e,idump_c3s2e,idump_vd3t4,idump_vd3h8,   &
     & idump_c3t4d,idump_diperm,idump_cldpt,idump_courant,              &
     & idump_dt_transp,idump_cfecdi,idump_cphco,idump_tsource,          &
     & idump_fluxhex8e,idump_nexday,idump_nexday_dec,idump_app_GC,      &
     & idump_sourma,idump_calro,idump_linearise,idump_init_sauve_mac6,  &
     & idump_lecmac5,idump_stomac5,idump_lecmac6,idump_stomac6,         &
     & idump_calsur,idump_connecte,idump_trunit,idump_c3s2d,            &
     & idump_h_anal_tran,idump_calvis,idump_fluxar3e,idump_homog_nonsat,&
     & idump_c3p6ens,idump_mailla,idump_mailla3d,idump_vol3h8,          &
     & idump_cal_vol_3d,idump_contou,idump_fluxdi,idump_c3q4d


      common /dump/idump_glob,idump_c3t3ens,idump_cteptns,idump_htosat, &
     &  idump_sattoh,idump_suint,idump_c3t3e,idump_c3t3d,idump_calfdi,  &
     & idump_c3h8d,idump_acmdi3,idump_inidtt,idump_cidpt,idump_ciept,   &
     & idump_inidte,idump_caldt,idump_c3t4e,idump_ctdpt,idump_vdetd,    &
     & idump_ctept,idump_ecperm,idump_sya2mo,idump_setgc,idump_setgc3d, &
     & idump_calfec,idump_trad_pot_libre,idump_starete,idump_up_t2,     &
     & idump_voisin,idump_tr_amont,idump_ch_el_amont,idump_cal_cupst,   &
     & idump_trav_source_lag,idump_int_source_lag,idump_acmdi,          &
     & idump_cagrad,idump_inter_t2d_o2,idump_inter_t2d_o3,idump_conlie, &
     & idump_interp,idump_vd3p6,idump_c3p6d,idump_acmec3,idump_acmec,   &
     & idump_c3p6e,idump_cagradel,idump_cieptns,idump_ts_ellam_tr,      &
     & idump_ts_ellam,idump_track_noeuds,idump_trclfc,idump_track_aretes&
     & ,idump_ch_tr_av,idump_setgc_ar,idump_ctept_efmh,idump_fluxec_efmh&
     & ,idump_fluxec,idump_fluxec_tri_efmh,idump_c3q4ens,idump_sGC,     &
     & idump_c3h8ens,idump_cleptns,idump_calfecns,idump_rpecns,         &
     & idump_cphetd,idump_cfecdins,idump_solgc,idump_fluxar3d,          &
     & idump_fluxec3d,idump_stface,idump_espace_mod,idump_clept,        &
     & idump_testhiter,idump_puits_psi_mod,idump_pot2pot_libre,         &
     & idump_inhibe_nds,idump_nd_from_dom,idump_masmai,idump_c3t4ens,   &
     & idump_c3s2ens,idump_c3h8e,idump_c3s2e,idump_vd3t4,idump_vd3h8,   &
     & idump_c3t4d,idump_diperm,idump_cldpt,idump_courant,              &
     & idump_dt_transp,idump_cfecdi,idump_cphco,idump_tsource,          &
     & idump_fluxhex8e,idump_nexday,idump_nexday_dec,idump_app_GC,      &
     & idump_sourma,idump_calro,idump_linearise,idump_init_sauve_mac6,  &
     & idump_lecmac5,idump_stomac5,idump_lecmac6,idump_stomac6,         &
     & idump_calsur,idump_connecte,idump_trunit,idump_c3s2d,            &
     & idump_h_anal_tran,idump_calvis,idump_fluxar3e,idump_homog_nonsat,&
     & idump_c3p6ens,idump_mailla,idump_mailla3d,idump_vol3h8,          &
     & idump_cal_vol_3d,idump_contou,idump_fluxdi,idump_c3q4d


        equivalence (idumps(1),idump_c3t3ens)

        save /dump/
