!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: GC_set_dump.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

subroutine GC_set_dump (nom,idump)

  implicit none

  include "dump.h"

  character*(*) nom

  integer idump,imp

  imp = 6

  if (nom.eq.'app_GC') then
     idump_app_GC = idump
     return
  endif

  if (nom.eq.'connecte') then
     idump_connecte = idump
     return
  endif

  if (nom.eq.'espace_mod') then
     idump_espace_mod = idump
     return
  endif

  if (nom.eq.'GC_sauve') then
     idump_sGC = idump
     return
  endif

  if (nom.eq.'init_sauve_mac6') then
     idump_init_sauve_mac6 = idump
     return
  endif

  if (nom.eq.'lecmac5') then
     idump_lecmac5 = idump
     return
  endif

  if (nom.eq.'lecmac6') then
     idump_lecmac6 = idump
     return
  endif

  if (nom.eq.'solgc') then
     idump_solgc = idump
     return
  endif

  if (nom.eq.'stomac5') then
     idump_stomac5 = idump
     return
  endif

  if (nom.eq.'stomac6') then
     idump_stomac6 = idump
     return
  endif
  write (imp,52) nom

52 format (//' Anomalie detectee par GC_set_dump:'//a// &
       &  ' Nom de sous-programme inconnu'//)

end subroutine GC_set_dump
