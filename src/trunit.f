        subroutine trunit (iul)

        include 'dump.h'
        include 'entsor.h'

c
c ce sous-programme cherche la premiere unite logique non utilisee
c a partir de iul+1 et la rretourne dans iul
        logical result
      common /versio/ ivers(200)
c        data ipass/1/
c        save ipass
      ivers(123) = 140391
	idump = idump_trunit
        iuldeb = iul+1
        do 100 iul=iuldeb,999
        inquire (unit=iul,opened=result)
        if (.not. result) goto 101
100     continue
        write (imp,*)
        write (imp,*)
        write (imp,*) ' erreur dans la recherche d''une unite logique'
        write (imp,*) ' disponible: pas d''unite entre',iuldeb,' et 999'
        call sortie ('trunit')
101     continue
	if (idump .ge. 1) write (imp,*) ' U.L. allouee par trunit',iul
        end
