       subroutine aparx(ntdl,mat4,mat5,rmat6,x,y)
c
c                    s .p. aparx
c                      -----------
c  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c  but : faire le produit d'une mtric a ou seuls
c  ----- les elements non nuls sont stockes (s.d.amat) par un vecteur x
c
c  parametres d entree :
c  ---------------------
c  ntdl   : ordre de la mtric a
c  mat4   : mat4(1)=0 mat4(i+1)=adresse du coefficient diagonal de la
c           ligne i dans les tableaux mat5 et a
c  mat5   : mat5(k)=no de la colonne du coefficient a(k)
c  rmat6   : la mtric
c  x      : vecteur(ntdl) a multiplier par a
c
c  parametre resultat :
c  --------------------
c  y      : vecteur y + a * x
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        include 'dprec.h'
       dimension mat4(*),mat5(*),x(*),y(*),rmat6(*)
c        data ipass/1/
c        save ipass
c       call appels ('aparx', ipass, 1)
       do   1 i=1,ntdl
       k1 = mat4(i)+1
       k2 = mat4(i+1)
       s = 0.0
       do   2 k=k1,k2
       j = mat5(k)
       s = s + rmat6(k) * x(j)
 2     continue
       y(i) = s
 1     continue
c       call appels ('aparx', ipass, 2)
       end
