!-------------------------------------------------------------------------------
! 
! LIBRARY NAME: libgc
! FILE NAME: tempo_mod.f90
! 
! CONTRIBUTORS: Patrick GOBLET, Nicolas FLIPO, Shuaitao WANG, 
!               Baptiste LABARTHE, Nicolas GALLOIS, Pierre GUILLOU
! 
! LIBRARY BRIEF DESCRIPTION: Linear solver based either on sparse 
! (https://www.netlib.org/sparse/readme (User guide available at 
! https://www.netlib.org/sparse/spdoc), or modulef (Copyright 
! (c) 1999 INRIA, https://www.rocq.inria.fr/modulef/english.html), 
! both EPL v2.0 friendly. 
!
! Library developed at the Geosciences Center, joint research center 
! of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
!
! COPYRIGHT: (c) 2022 Contributors to the libgc Library.
! CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
!          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
!
! All rights reserved. This Library and the accompanying materials
! are made available under the terms of the Eclipse Public License v2.0
! which accompanies this distribution, and is available at
! http://www.eclipse.org/legal/epl-v20.html
! 
!------------------------------------------------------------------------------*/
!
! This file comes from the MODULEF library distributed by INRIA, but 
! has been modified afterwards.
!
!------------------------------------------------------------------------------*/

module tempo_mod

integer(4),parameter :: max_noms_fichiers = 100
integer(4),save :: n_noms_fichiers = 0
character(100) noms_fichiers (max_noms_fichiers)
integer(4) iul_fichiers (max_noms_fichiers)

contains

   subroutine ajoute_nom_fichier (nom,iul)

	implicit none
	character*(*) :: nom
	integer(4) iul
	include 'entsor.h'

	n_noms_fichiers = n_noms_fichiers + 1
	if (n_noms_fichiers .gt. max_noms_fichiers) then
	   write (imp,*) ' Liste des sauvegardes de fichiers saturee'
	   write (imp,*) ' Augmenter le parametre max_noms_fichiers'
	   write (imp,*) ' (module tempo_mod.f90)'
	   call sortie ('ajoute_nom_fichier')
	endif
	noms_fichiers(n_noms_fichiers) = trim(nom)
	iul_fichiers(n_noms_fichiers) = iul
        wRite (6,*) n_noms_fichiers,trim(nom)

   end subroutine ajoute_nom_fichier

   

end module tempo_mod
